-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-09-2020 a las 16:23:20
-- Versión del servidor: 5.7.31-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mlm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_align` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `photo`, `button_text`, `button_link`, `text_align`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'Emprendimiento de negocios.', 'Expertos en negocios internacionales ponen a disposicion la mejor plataforma.', '1598736872573banners.png', 'Registrarme ahora!!', '/register', 'right', 1, '2020-08-29 21:34:32', '2020-08-30 05:40:10'),
(2, 'Proyecto 3x12', 'La gran plataforma de emprendimiento para lograr tus sueños', '1598736927957banners.png', 'Quiero saber más', '/contenido/about-2', 'left', 1, '2020-08-29 21:35:27', '2020-08-30 05:40:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `create_date`, `createdAt`, `updatedAt`) VALUES
(8, 'Category#1', 'Category#1', '2020-08-18 20:58:47', '2020-08-02 00:00:00', '2020-08-02 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `email` varchar(300) NOT NULL,
  `port` int(10) DEFAULT '587',
  `host` varchar(300) DEFAULT 'smtp.gmail.com',
  `pass` varchar(300) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `proyect` varchar(300) NOT NULL,
  `twitter` text,
  `instagram` text,
  `facebook` text,
  `correo_admin` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `email`, `port`, `host`, `pass`, `phone`, `address`, `proyect`, `twitter`, `instagram`, `facebook`, `correo_admin`) VALUES
(1, 'noreply@webbost.net', 587, 'smtp.gmail.com', '1Q2w3e4r5t', '+57 311 222 3334', '5678 Bangla Main Road, cities 580', '3x12', 'http://twitter.com/1', 'http://instagram.com/1', 'http://facebook.com/1', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents`
--

CREATE TABLE `contents` (
  `id` int(10) NOT NULL,
  `parentid` int(10) DEFAULT NULL,
  `typeid` int(10) DEFAULT NULL,
  `menu` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `photo_banner` text,
  `photo` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `button_text` varchar(200) DEFAULT NULL,
  `button_link` varchar(300) DEFAULT NULL,
  `text_align` varchar(20) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contents`
--

INSERT INTO `contents` (`id`, `parentid`, `typeid`, `menu`, `title`, `description`, `content`, `photo_banner`, `photo`, `status`, `button_text`, `button_link`, `text_align`, `created`, `createdAt`, `updatedAt`) VALUES
(1, 0, 1, 'La empresa', 'Titulo empresa', 'prueba', '<p>prueba3</p>', NULL, '', 1, NULL, NULL, NULL, '2020-07-31 21:13:07', '2020-08-02 18:42:21', '2020-08-02 18:42:33'),
(2, 0, 1, 'About', 'About', 'asd2', '<p>asd22222</p>', NULL, 'logo-symbol-dark1.png', 1, NULL, NULL, NULL, '2020-08-01 22:32:53', '2020-08-02 18:42:21', '2020-08-02 18:42:33'),
(3, 1, 1, 'Politicas de privacidad', 'Politicas de privacidad', 'ascds', '<p>text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo text demo </p>', NULL, '2.jpeg', 0, NULL, NULL, '', '2020-08-01 22:34:36', '2020-08-02 18:42:21', '2020-08-02 18:42:33'),
(26, 1, 1, 'prueba444', 'prueba', 'te', '<p>tesfsa</p>', NULL, '', 0, NULL, NULL, '', '2020-08-04 15:43:36', '2020-08-04 00:00:00', '2020-08-04 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contents_type`
--

CREATE TABLE `contents_type` (
  `id` int(10) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contents_type`
--

INSERT INTO `contents_type` (`id`, `type`) VALUES
(1, 'Menus'),
(2, 'Banner'),
(3, 'Seccion 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

CREATE TABLE `diccionario` (
  `id` int(11) NOT NULL,
  `keyd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valued` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `diccionario`
--

INSERT INTO `diccionario` (`id`, `keyd`, `valued`, `createdAt`, `updatedAt`) VALUES
(1, 'titulo_1_seccion_1', 'Estrategias de mercadeo', '2020-08-26 21:56:59', '2020-08-26 21:56:59'),
(2, 'grandganancias', 'Grandes ganancias', '2020-08-26 21:57:40', '2020-08-26 21:57:40'),
(3, 'titulo_2_seccion_1', 'Grandes Ganacias', '2020-08-30 02:16:28', '2020-08-30 02:16:28'),
(4, 'titulo_3_seccion_1', 'Redes de mercadeo', '2020-08-30 02:16:50', '2020-08-30 02:16:50'),
(5, 'texto_1_seccion_1', 'Expertos en el mercado de la tecnologia y la comunicación te ayudará a crecer', '2020-08-30 02:17:42', '2020-08-30 02:17:42'),
(6, 'texto_2_seccion_1', 'Empieza a crecer con nosotros, nuestra plataforma, nuestras metas.', '2020-08-30 02:17:58', '2020-08-30 02:17:58'),
(7, 'texto_3_seccion_1', 'Sé un lider desde ya, piensa como emprendedor y toma acción.', '2020-08-30 02:18:39', '2020-08-30 02:18:39'),
(8, 'noticias_novedades', 'Noticias y Novedades', '2020-08-30 02:21:04', '2020-08-30 02:21:04'),
(9, 'detalle_noticias_novedades', 'Aqui te mostraremos lo ultimo en noticias y novedades de 3x12', '2020-08-30 02:25:38', '2020-08-30 02:25:38'),
(10, 'seccion_final_peq', 'ESTÁS LISTO PARA...', '2020-08-30 02:27:28', '2020-08-30 02:27:28'),
(11, 'seccion_final', 'Hacer crecer tu negocio?', '2020-08-30 02:27:42', '2020-08-30 02:27:42'),
(12, 'texto_boton_seccion_final', 'Registrate ahora', '2020-08-30 02:28:18', '2020-08-30 02:30:50'),
(13, 'seccion_final_grande', 'Hacer crecer tu negocio?', '2020-08-30 02:34:06', '2020-08-30 02:34:06'),
(14, 'faqs', 'Preguntas Frecuentes', '2020-08-30 03:15:14', '2020-08-30 03:15:14'),
(15, 'footer', 'Copyright © 2020 3x12. Todos los derechos reservados.', '2020-08-30 03:19:02', '2020-08-30 03:19:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `create_date`, `createdAt`, `updatedAt`) VALUES
(2, '¿Qué pasa sino hago la recarga de B/15 en un mes?', 'Pierde el beneficio de recibir el pago del residual, no se acumula. Si ese mes cumple los puntos para un rango, no lo gana, ni los demás bonos.', '2020-08-30 02:49:35', NULL, '2020-08-30 02:49:35'),
(3, '¿Qué pasa si en un mes solo recargo B/10?', 'Pierde el beneficio de recibir el pago del residual, no se acumula. Si ese mes cumple los puntos para un rango, no lo gana, ni los demás bonos.', '2020-08-30 02:49:45', NULL, '2020-08-30 02:49:45'),
(4, '¿Cuántos puestos puedo adquirir en la red de 3X12 a mi nombre?', 'Hasta 4 puestos por Identificación (cédula o pasaporte) en cualquier posición.', '2020-08-30 02:50:15', '2020-08-30 02:50:15', '2020-08-30 02:50:15'),
(5, '¿Qué pasa si mi red crece en profundidad y yo solo he vinculado 2 directos?', 'Hasta tanto no vincule 3 Directos, no recibirá ningún beneficio económico de 3X12, inclusive los Bonos rápidos.', '2020-08-30 02:50:36', '2020-08-30 02:50:36', '2020-08-30 02:50:36'),
(6, '¿Dónde puedo hacer la recarga mensual?', 'Se descontará de las comisiones y bonos a favor en cada mes, de no tener saldo a favor, podrá pagarla por Banca en Línea, ACH, depósito en las cuentas de MOVILGATE o en efectivo en a oficina de 3X12. Igualmente la podrá adquirir en cualquier tienda o comercio en la calle debiendo enviar el recibo por Correo electrónico.', '2020-08-30 02:50:54', '2020-08-30 02:50:54', '2020-08-30 02:50:54'),
(7, '¿Puedo hacer Portabilidad de mi número celular?', 'Si. Para vincularse a 3X12 lo podrá hacer acogiéndose a los procedimientos y documentación respectivos. Estando en 3X12 no podrá portarse a otro Operador sopena de perder los beneficios y el puesto en la red.', '2020-08-30 02:51:16', '2020-08-30 02:51:16', '2020-08-30 02:51:16'),
(8, '¿Qué pasa si en un mes logro el rango de Master y en el siguiente no hago el puntaje?', 'Para obtener los beneficios de un Rango, debe lograr el puntaje requerido en cada mes, de lo contrario se le liquidará de acuerdo con los puntos que obtenga en el mes.', '2020-08-30 02:51:31', '2020-08-30 02:51:31', '2020-08-30 02:51:31'),
(9, '¿Los puntos para los ascensos son acumulables en los meses?', ' Los únicos puntos acumulables en el tiempo son los de los Kit de inicio, formación de líderes y vinculados directos.', '2020-08-30 02:51:47', '2020-08-30 02:51:47', '2020-08-30 02:51:47'),
(10, '¿Qué gano si en un mes, una persona que está debajo de mi recarga los B/15 y adicional en el mismo mes otra de B/10?', 'Toda la estructura ascendente linealmente recibe el porcentaje respectivo según el rango en que se encuentre ese mes. (Básico = 2%)', '2020-08-30 02:52:02', '2020-08-30 02:52:02', '2020-08-30 02:52:02'),
(11, '¿Qué pasa si no coloco Matriculador en el Registro?', 'Sino coloca Matriculador el sistema automáticamente le asigna el primer cupo disponible vacío, de izquierda a derecha, que tenga la estructura general de 3X12.', '2020-08-30 02:52:53', '2020-08-30 02:52:19', '2020-08-30 02:52:53'),
(12, '¿Puedo hacer la recarga de B/15 fraccionada durante el mes?', 'Si siempre y cuando complete los B/15 entre el 6 y el 5 de cada mes.', '2020-08-30 02:52:59', '2020-08-30 02:52:59', '2020-08-30 02:52:59'),
(13, '¿Cuándo empiezo a ganar plata?', 'Una vez vincule los 3 directos y estos a su vez hagan lo mismo y así sucesivamente hasta generar en Bono Rápido mínimo B/30 para pagarle B/15 por este concepto y descontarle el valor de la primera recarga o hasta generar el primer residual mensual por recargas siempre y cuando haya vinculado 3 directos y realice su recarga mensual de B/15 ', '2020-08-30 02:53:22', '2020-08-30 02:53:22', '2020-08-30 02:53:22'),
(14, '¿El kit lo puede adquirir un menor de edad?', 'No. solamente mayores de edad', '2020-08-30 02:53:31', '2020-08-30 02:53:31', '2020-08-30 02:53:31'),
(15, '¿El Kit lo puede comprar una persona que vive en Venezuela?', 'No. Debe residir en Panamá', '2020-08-30 02:53:41', '2020-08-30 02:53:41', '2020-08-30 02:53:41'),
(16, '¿Este Kit me lo facturan?', 'Si el cliente lo solicita Si', '2020-08-30 02:53:51', '2020-08-30 02:53:51', '2020-08-30 02:53:51'),
(17, '¿Qué impuesto pago por el Kit de Comunicaciones?', '7% por impuesto ITBMS', '2020-08-30 02:54:02', '2020-08-30 02:54:02', '2020-08-30 02:54:02'),
(18, '¿Puedo comprar el Kit estando en San Blas?', 'En la primera etapa no. A partir de noviembre en la segunda etapa si', '2020-08-30 02:54:17', '2020-08-30 02:54:17', '2020-08-30 02:54:17'),
(19, '¿La Simcard del Kit es solo de Claro?', 'Si solamente', '2020-08-30 02:54:24', '2020-08-30 02:54:24', '2020-08-30 02:54:24'),
(20, '¿Cuáles son las fechas de corte?', 'Para 3X12 la fecha de corte será el 5 de cada mes. Importante tener en cuenta que no es la misma a la de la recarga de los Super Pack.', '2020-08-30 02:54:40', '2020-08-30 02:54:40', '2020-08-30 02:54:40'),
(21, '¿Porqué debo cambiar la simcard y el número cada 6 meses?', 'Para garantizar el pago de los beneficios económicos ofrecidos por 3X12 provenientes del Operador celular.', '2020-08-30 02:54:58', '2020-08-30 02:54:58', '2020-08-30 02:54:58'),
(22, '¿Cuántas veces puedo ganar el bono de cada rango?', 'Una sola Vez por Rango', '2020-08-30 02:55:08', '2020-08-30 02:55:08', '2020-08-30 02:55:08'),
(23, '¿Puedo hacer todo el proceso de vinculación, registro y pago por internet?', 'Si todo', '2020-08-30 02:55:16', '2020-08-30 02:55:16', '2020-08-30 02:55:16'),
(24, '¿En cuánto tiempo me envía el Kit?', 'Durante la Primera Etapa, en Ciudad de Panamá en máximo 24 horas. En Panamá Oeste hasta Coclé y Colón, en máximo 72 horas', '2020-08-30 02:55:31', '2020-08-30 02:55:31', '2020-08-30 02:55:31'),
(25, '¿Puedo tener más de 3 Directos?', 'Si todos los que desee. Lo que no puede tener es más de 3 Frontales o líneas.', '2020-08-30 02:55:42', '2020-08-30 02:55:42', '2020-08-30 02:55:42'),
(26, '¿El hecho de que una persona de mi estructura no recargue los B/15 en qué me afecta para mis pagos y los de la estructura?', 'Para nada, solo quien no recarga no recibe ningún pago, pero no afecta los pagos de la estructura.', '2020-08-30 02:55:50', '2020-08-30 02:55:50', '2020-08-30 02:55:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`id`, `category_id`, `name`, `description`, `create_date`, `createdAt`, `updatedAt`) VALUES
(23, 5, '1598765586858gallery.png', 'imagen de galeria', '2020-08-30 05:33:06', '2020-08-30 05:33:06', '2020-08-30 07:55:18'),
(24, 5, '1598766898913gallery.png', 'imagen galeria', '2020-08-30 05:54:58', '2020-08-30 05:54:58', '2020-08-30 07:55:27'),
(25, 4, '1598767139134gallery.png', 'imagen galeria', '2020-08-30 05:58:59', '2020-08-30 05:58:59', '2020-08-30 07:55:31'),
(26, 4, '1598767144592gallery.png', 'imagen galeria', '2020-08-30 05:59:04', '2020-08-30 05:59:04', '2020-08-30 07:55:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery_category`
--

CREATE TABLE `gallery_category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gallery_category`
--

INSERT INTO `gallery_category` (`id`, `name`, `description`, `create_date`, `createdAt`, `updatedAt`) VALUES
(4, 'Album paisajes', 'demo esto es una pruebas', '2020-08-08 02:22:36', '2020-08-06 00:00:00', '2020-08-30 06:30:19'),
(5, 'otro album', 'prueba', '2020-08-08 02:22:45', '2020-08-04 00:00:00', '2020-08-05 00:00:00'),
(6, 'album nuevo', 'album nuevo', '2020-08-30 06:30:46', '2020-08-30 06:30:46', '2020-08-30 06:30:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `member_log`
--

CREATE TABLE `member_log` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent_id` int(10) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `member_log`
--

INSERT INTO `member_log` (`id`, `name`, `parent_id`, `createdAt`, `updatedAt`) VALUES
(133, 'Admin MLM', 401, '2020-08-21 00:01:36', '2020-08-31 00:58:09'),
(335, 'pruba2', 133, '2020-08-19 00:01:36', '2020-08-17 00:01:36'),
(344, 'prueba3', 335, '2020-08-21 00:02:29', '2020-08-11 00:02:29'),
(345, 'Usuario de prueba', 344, '2020-08-21 00:02:46', '2020-08-31 01:49:19'),
(354, 'MIYER ARANGO', 345, '2020-08-21 00:03:37', '2020-08-28 18:49:27'),
(397, 'Pedro Peres', 354, '2020-08-28 15:18:22', '2020-08-28 15:18:25'),
(398, 'MARIA GONZALEZ', 354, '2020-08-28 20:02:13', '2020-08-28 20:02:21'),
(399, 'jaun garcia', 398, '2020-08-28 22:30:08', '2020-08-28 22:30:12'),
(400, 'MARCOS CAMERA', 399, '2020-08-28 22:52:04', '2020-08-28 22:52:12'),
(402, 'LUIS RIOS', 133, '2020-08-31 13:09:50', '2020-08-31 13:09:54'),
(403, 'KARLA DUARTE', 133, '2020-08-31 13:11:44', '2020-08-31 13:11:55'),
(404, 'STEVEN APONTE', 403, '2020-08-31 13:17:56', '2020-08-31 13:17:58'),
(405, 'CAMILO TORRES', 344, '2020-09-01 14:57:38', '2020-09-01 14:57:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `news` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `photo` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `related_news` varchar(300) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`id`, `news`, `title`, `description`, `content`, `photo`, `status`, `related_news`, `created`, `createdAt`, `updatedAt`) VALUES
(1, 'Urshela sigue conectado', 'Urshela sigue conectado en la ofensiva de los Yankees', 'Urshela sigue conectado en la ofensiva de los Yankees', '<p>Aún se escucha el eco del <a href=\"https://www.eltiempo.com/deportes/otros-deportes/grand-slam-de-giovanny-urshela-con-los-yankees-524984\"><strong>primer grand slam del colombiano Giovanny Urshela</strong></a> con los Yankees, en la victoria del sábado 5-2 contra los Medias Rojas de Boston. Este domingo no conectó jonrón, pero volvió a ser muy importante en la ofensiva de su equipo.</p><p>Yankees venció este domingo 9-7 a Boston, en un partido en el que el cartagenero se fue con tres imparables y una carrera impulsada en cuatro turnos al bate.<br><br>(Lea también:<a href=\"https://www.eltiempo.com/deportes/otros-deportes/donovan-solano-lider-de-carreras-impulsadas-en-las-grandes-ligas-525196\"><strong> El impresionante momento de Donovan Solano en las Grandes Ligas)</strong></a><br><br>Urshela subió su promedio ofensivo en esta temporada a .308, con ocho imparables en 26 turnos, seis carreras impulsadas y seis anotadas.<br><br>Con la victoria frente a Medias Rojas, Yankees se mantuvo en la punta de la división este de la liga Americana, con marca de siete victorias y una derrota.</p>', '1598746543402news.png', 1, '', '2020-08-30 00:15:43', '2020-08-30 00:15:43', '2020-08-30 00:15:43'),
(27, 'coronavirus béisbol', 'El coronavirus sigue amenazando al béisbol: hay nuevos casos', 'El coronavirus sigue amenazando al béisbol: hay nuevos casos\r\n', '<p>Los casos de<a href=\"https://www.eltiempo.com/noticias/covid-19\"><strong> coronavirus</strong></a> en la organización de los<strong> Cardenales de San Luis</strong> se incrementaron el sábado, por lo que el juego entre esa novena y los <strong>Cerveceros de Milwaukee</strong> se suspendió.</p><p>De acuerdo con varios informes,<strong> tres miembros del personal de los Cardenales dieron positivo para coronavirus en las pruebas rápidas. </strong>Mientras, las Grandes Ligas indicaron que el equipo espera los resultados completos de las pruebas de saliva más tarde el sábado.</p><p>El viernes por la noche el comisionado de las Grandes Ligas, Rob Manfred, le dijo al director ejecutivo de la Asociación de Jugadores, Tony Clark, que <strong>si el deporte no hace un mejor trabajo en el manejo del coronavirus, podría suspenderse la temporada.</strong></p><p>En conversaciones de texto con sus compañeros de equipo el viernes, los representantes de jugadores de la Asociación también advirtieron sobre la amenaza para la temporada, según las mismas fuentes.<br><br><strong>Pero el sábado Manfred dijo que sigue confiando en que la temporada 2020 puede continuar,</strong> y declaró que \"no hay razón para renunciar (a la actividad) ahora\".</p><p><strong>El primer partido de la serie del viernes fue pospuesto después de que dos jugadores de los Cardenales dieron positivo al coronavirus.</strong> El juego se programó para formar parte de una doble jornada el domingo, la primera en jugarse a siete episodios por partido tras un acuerdo anunciado por las mayores.</p><p>Sin embargo, después de enterarse de los dos primeros casos positivos el jueves por la noche, los Cardenales dijeron que instruyeron a todos los jugadores y al personal para que se autoaislaran en sus habitaciones de hotel en Milwaukee hasta nuevo aviso</p>', '1598767366106news.png', 1, '1,29', '2020-08-30 06:02:46', '2020-08-30 06:02:46', '2020-08-30 06:02:46'),
(29, 'Egan  Nairo ', 'Así les fue a Egan y Nairo ', 'Así les fue a Egan y Nairo ', '<p><strong>Andrea Bagioli</strong> se impuso en la primera etapa del<strong> Tour de L’Ain </strong>y es líder de la competencia, un tramo entre Montréal-la-Cluse y Ceyzeriat, en la que las figuras colombianas <a href=\"https://www.eltiempo.com/noticias/egan-bernal\">Egan Bernal</a> y <a href=\"https://www.eltiempo.com/noticias/nairo-quintana\">Nairo Quintana</a>, terminaron en el lote principal. </p><p>De igual manera, figuras como <strong>Chris Froome, Geraint, homas, Primoz Roglic, Tom Dumoulin y Steven Kruijswijk </strong>tampoco tuvieron inconvenientes durante el día.<br><strong>Egan Bernal es 14 y Nairo 23,</strong> ambos a 10 segundos del líder.<br><br><strong>La carrera tuvo cuatro emociones, premios de montaña de cuarta categoría, que sirvieron para mover esa clasificación,</strong> pues los escaladores grandes no se inquietaron.<br><br>Una fuga de cuatro ciclistas animó la jornada. Los integrantes fueron <strong>Alexys Brunel, Michal Paluta, Martin Salmon e Iván Centrone, </strong>quienes se mostraron.<br><br>Roglic apretó el paso a 2 kilómetros del final, el lote se estiró y Bernal se le pegó a la rueda, luego todo se calmó. </p><p> </p><p><strong>Los escapados tuvieron que recorrer muchos kilómetros en busca de la victoria, </strong>pero cuando la jornada entró en los últimos 30 kilómetros la diferencia comenzó a bajar y fueron cazados. <br><br><strong>Este sábado la etapa será de 140 kilómetros y será entre Lagnieu-Lelex Monts-Jura, con cinco pasos montañosos, </strong>uno de tercera y el resto de segunda, el último en la meta, donde podrían marcarse las primeras diferencias.</p>', '1598767478306news.png', 1, '1,27', '2020-08-30 06:04:38', '2020-08-30 06:04:38', '2020-08-30 06:04:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` text NOT NULL,
  `member_id` text NOT NULL,
  `member_name` text NOT NULL,
  `product_id` text NOT NULL,
  `qty` text NOT NULL,
  `unit_price` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `primerpago` int(2) NOT NULL DEFAULT '0',
  `create_date` text NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `member_id`, `member_name`, `product_id`, `qty`, `unit_price`, `status`, `primerpago`, `create_date`, `updatedAt`, `createdAt`) VALUES
(255, '03441594778533', '344', 'Pedro', '359', '1', '10000', 'paid', 0, '2020-07-15 02:02:13', '2020-08-02 18:45:44', '2020-08-02 18:45:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `type` text NOT NULL,
  `ProductName` text NOT NULL,
  `ProductCategory` text NOT NULL,
  `Tax` text NOT NULL,
  `sgst` float NOT NULL,
  `cgst` float NOT NULL,
  `igst` float NOT NULL,
  `Available_qty` int(10) NOT NULL,
  `SKU` text NOT NULL,
  `Price` int(11) NOT NULL,
  `hsn` varchar(20) NOT NULL,
  `sac` varchar(20) NOT NULL,
  `SalePrice` int(11) NOT NULL,
  `description` text NOT NULL,
  `productImage` text NOT NULL,
  `company_name` text NOT NULL,
  `company_email` text NOT NULL,
  `company_phone` text NOT NULL,
  `City` text NOT NULL,
  `State` text NOT NULL,
  `Pincode` text NOT NULL,
  `company_address` text NOT NULL,
  `ip_address` text NOT NULL,
  `create_date` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `user_id`, `type`, `ProductName`, `ProductCategory`, `Tax`, `sgst`, `cgst`, `igst`, `Available_qty`, `SKU`, `Price`, `hsn`, `sac`, `SalePrice`, `description`, `productImage`, `company_name`, `company_email`, `company_phone`, `City`, `State`, `Pincode`, `company_address`, `ip_address`, `create_date`, `createdAt`, `updatedAt`) VALUES
(234261, 0, '', '15USD', 'Category#1', '', 5, 2, 1, 10, 'KJO8754', 15000, '', '', 15000, 'product desctrion ', 'test_-_Copy_(2).png', '', '', '', '', '', '', '', '201.244.162.124', '2020-08-01 22:09:27', '2020-08-02 18:46:27', '2020-08-02 18:46:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchase_products`
--

CREATE TABLE `purchase_products` (
  `id` int(11) NOT NULL,
  `qty` int(10) NOT NULL,
  `unit_price` int(10) NOT NULL,
  `Tax` text NOT NULL,
  `description` text NOT NULL,
  `userid` int(11) NOT NULL,
  `ip_address` text NOT NULL,
  `create_date` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `value` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `type`, `value`, `createdAt`, `updatedAt`) VALUES
(1, 'tax', '5', '2020-08-02 18:48:26', '2020-08-02 18:48:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `role` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `first_name` text,
  `last_name` text,
  `email` text NOT NULL,
  `alternateEmail` text,
  `mobile_no` text,
  `language` text,
  `website` text,
  `picture_url` text,
  `profile_url` text,
  `dob` text,
  `gender` text,
  `about` text,
  `locale` text,
  `designation` text,
  `address` text,
  `country` text,
  `city` text,
  `pincode` text,
  `vat_number` text,
  `AccountNumber` text,
  `IFSCCode` text,
  `ip_address` text,
  `created` text COMMENT 'Fecha inscripcion',
  `iccid` varchar(20) DEFAULT NULL COMMENT '01235465489465465465',
  `imei` varchar(15) DEFAULT NULL COMMENT '012354654894654',
  `portabilidad` int(2) DEFAULT NULL COMMENT '1=si, 0=no',
  `numero` varchar(15) DEFAULT NULL COMMENT '50762011111',
  `operador_donante` varchar(50) DEFAULT NULL,
  `tipo_identificacion` int(2) DEFAULT NULL COMMENT 'tipo identificacion',
  `nacionalidad` varchar(100) DEFAULT NULL COMMENT 'nacionalidad /panameño, colombiano, etc',
  `ciudad` varchar(100) DEFAULT NULL COMMENT 'ciudad',
  `corregimiento` varchar(100) DEFAULT NULL COMMENT 'departamento',
  `barriada` varchar(100) DEFAULT NULL COMMENT 'barrio',
  `calle` varchar(100) DEFAULT NULL COMMENT 'direccion',
  `casa` varchar(100) DEFAULT NULL COMMENT '??',
  `ph` varchar(100) DEFAULT NULL COMMENT '???',
  `apartamento` varchar(40) DEFAULT NULL COMMENT '??',
  `aclaracion` varchar(50) DEFAULT NULL COMMENT '??',
  `numcelularllamadas` int(15) DEFAULT NULL COMMENT 'num cel solo llamadas',
  `numcelularwhatsapp` int(15) DEFAULT NULL COMMENT 'numcel de solo whatsapp',
  `numotro` int(15) DEFAULT NULL COMMENT 'num telefono otro',
  `banco` varchar(255) DEFAULT NULL COMMENT 'nombre del banco',
  `numerocuenta` varchar(50) DEFAULT NULL COMMENT 'numero de cuenta del banco',
  `ifpago` int(2) NOT NULL DEFAULT '0',
  `ifpago_fecha` datetime DEFAULT NULL,
  `lastlogged` text,
  `modified` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `facebookId` varchar(1000) DEFAULT NULL,
  `reset` varchar(255) DEFAULT NULL,
  `datereset` varchar(255) DEFAULT NULL,
  `Matriculador` int(11) DEFAULT NULL,
  `Patrocinador` int(11) DEFAULT NULL,
  `numidentificacion` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `status`, `name`, `first_name`, `last_name`, `email`, `alternateEmail`, `mobile_no`, `language`, `website`, `picture_url`, `profile_url`, `dob`, `gender`, `about`, `locale`, `designation`, `address`, `country`, `city`, `pincode`, `vat_number`, `AccountNumber`, `IFSCCode`, `ip_address`, `created`, `iccid`, `imei`, `portabilidad`, `numero`, `operador_donante`, `tipo_identificacion`, `nacionalidad`, `ciudad`, `corregimiento`, `barriada`, `calle`, `casa`, `ph`, `apartamento`, `aclaracion`, `numcelularllamadas`, `numcelularwhatsapp`, `numotro`, `banco`, `numerocuenta`, `ifpago`, `ifpago_fecha`, `lastlogged`, `modified`, `createdAt`, `updatedAt`, `facebookId`, `reset`, `datereset`, `Matriculador`, `Patrocinador`, `numidentificacion`) VALUES
(133, 'admin@mlm.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', '1', 1, 'Admin MLM', 'Admin', 'MLM', 'admin@mlm.com', '', '9785834812', '', '', '1598972203746user.png', '', '', '', 'about applicaiton', '', '', 'VDN Jaipur', 'india', 'JAIPUR', '332710', '', '', '', '::1', '2018-01-06 13:23:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '11-08-2020 18:22 PM', '2018-06-01 08:08:38', '2020-07-30 05:27:33', '2020-09-01 14:56:43', NULL, NULL, NULL, 401, 401, 0),
(335, 'test@gmail.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeLoyYG/tmPfdXxuFRSkC2pxeIfZSJYyq', '0', 1, 'Hiten Pingolia', '', '', 'test@gmail.com', '', '9785834812', '', '', 'Hiten_Pingolia_n_2.jpg', '', '', '', '', '', '', 'VDN, Jaipur', '', '', '302039', '', '', '', '::1', '2018-06-01 08:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', '', '2020-07-30 05:27:33', '2020-08-04 00:00:00', NULL, NULL, NULL, NULL, NULL, 0),
(344, 'testdem@gmail.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1Pev4z2CFxf0nE/sFYNaOkuUxj7SY.H9he', 'Customer', 0, 'Gonzo', '', '', 'testdem@gmail.com', '', '3102220811', '', '', '', '', '', '', '', '', '', 'calle demo', '', '', '302039', '', 'nose', '1111', '::1', '2020-07-15 02:02:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', '', '2020-07-30 05:27:33', '2020-08-04 00:00:00', NULL, NULL, NULL, NULL, NULL, 0),
(345, 'eimepe@yahoo.com', '$2a$10$6fWceLXoFRPQqLaV.7ikuuo3Gjx.q2Bf9J0CMHHRHsKfzyc6GHZbG', '0', 3, 'Usuario de prueba', 'Admin', 'MLM', 'eimepe@yahoo.com', '', '9785834812', '', '', '1598602540423user.png', '', '', '', 'about applicaiton', '', '', 'VDN Jaipur', 'india', 'JAIPUR', '332711', '', '', '', '::1', '2018-01-06 13:23:35', NULL, NULL, NULL, '123456', NULL, 2, 'Colombia', 'bogota', 'dfdf', 'Jxuf', NULL, NULL, NULL, NULL, NULL, 12345, 12345, 313722218, 'Jdjdj gnekwkwk d d d  d', '123452994949393992929292', 1, '2020-08-24 04:59:29', '06-08-2020 18:14 PM', '2018-06-01 08:08:38', '2020-07-30 05:27:33', '2020-08-31 01:49:18', '10219174386893492', '9451', '1598768587023', 344, 133, 12345678),
(354, 'marango@celairis.net', '$2a$10$CK1iPQ216QBDYe1rVjpAmuHrRHVfCNNdRHm95wwyAHOPUzj/XlYpK', '0', 3, 'MIYER ARANGO', 'MIYER', 'ARANGO', 'marango@celairis.net', NULL, '62560099', NULL, NULL, '1598495032308user.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '62560099', NULL, 2, 'Colombiano ', 'Ciudad de panama', 'Sanfelipe', 'Casco antiguo', 'Calle 2 oeste ', 'maralta', 'Maralta ', '4', 'Cerca al farma Revilla ', 62560099, 62560099, NULL, 'Banco General', '1234557', 1, '2020-08-24 00:49:15', NULL, NULL, '2020-08-11 00:00:00', '2020-08-28 22:06:29', NULL, '0', '1598228831477', 345, 345, 248445),
(396, 'CORREO@FGSJD.COM', '$2a$10$ZTjBhMLisTM0MZMW8cwbfeTDZgU4RVn2S1DrfnhPW/zUS/YoUOxoW', '0', 0, 'MARIA  DUARTE', NULL, NULL, 'CORREO@FGSJD.COM', NULL, '6235486', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-08-28 14:59:49', '2020-08-30 07:49:22', NULL, NULL, NULL, NULL, NULL, NULL),
(397, 'ecortesia@celairis.net', '$2a$10$7xl4CsHdeVXkBKn3A0g5c.Q3EhqCd0F7PGX2tdVSyvhc/JsG8ZpwK', '0', 3, 'Pedro Peres', NULL, NULL, 'ecortesia@celairis.net', NULL, '62560091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-28 15:21:12', NULL, NULL, '2020-08-28 15:17:24', '2020-08-28 15:21:12', NULL, NULL, NULL, 354, 354, NULL),
(398, 'jairis.gonzalez@celairis.net', '$2a$10$7lmskiuZf0WbYCzciIi/tOPiLvq/K.zGN5t5MXh1rtDD7YB5snJa2', '0', 3, 'MARIA GONZALEZ', NULL, NULL, 'jairis.gonzalez@celairis.net', NULL, '62560092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-28 20:58:22', NULL, NULL, '2020-08-28 20:00:23', '2020-08-28 20:58:23', NULL, NULL, NULL, 354, 354, NULL),
(399, 'correo@gmail.com', '$2a$10$/Wx0UG2cTkEwaKhgjRQBouItY8e6cDcSLa7/DcnCiHM9/YPheXZc.', '0', 3, 'jaun garcia', NULL, NULL, 'correo@gmail.com', NULL, '62560095', NULL, NULL, '1598655959628user.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Colombiano', 'Ciudad de Panamá ', 'San Felipe ', 'Casco antiguo', 'Calle 2oeste', 'Casa 1', NULL, '4', 'El mejor lugar de Panamá ', 62560099, 62560099, 62560099, 'Banco general', '1236548970', 1, '2020-08-28 22:36:38', NULL, NULL, '2020-08-28 22:27:08', '2020-08-28 23:17:06', NULL, NULL, NULL, 398, 398, 139919804),
(400, 'CORREO1@GMAIL.COM', '$2a$10$Hop0O1rZi3BccYIEzori6uKJDlAPXmwwVkfmICsHXLxuhbppNfE5a', '0', 3, 'MARCOS CAMERA', NULL, NULL, 'CORREO1@GMAIL.COM', NULL, '62560095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-08-28 22:53:50', NULL, NULL, '2020-08-28 22:51:15', '2020-08-28 22:53:50', NULL, NULL, NULL, 399, 399, NULL),
(401, 'pruebaeimepe3@gmail.com', '$2a$10$ZIEvhSf7ZqRIZuXG2MleIeADqVBAGfDaktVDIgyjq/9CsvEhXBWsG', '0', 0, 'Eider Mejia sdsd', NULL, NULL, 'pruebaeimepe3@gmail.com', NULL, '3138217514', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-08-30 06:21:06', '2020-08-30 06:21:06', NULL, NULL, NULL, NULL, NULL, NULL),
(402, 'CORREO3@GMAIL.COM', '$2a$10$v9593zf4OXRnpESdD7mZJuGYKZN2HNWyOSIgd6.oLy4g1S0JuVRdW', '0', 0, 'LUIS RIOS', NULL, NULL, 'CORREO3@GMAIL.COM', NULL, '25468578', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-08-31 00:50:45', '2020-08-31 13:09:54', NULL, NULL, NULL, 133, 133, NULL),
(403, 'correo4@gmail.com', '$2a$10$NY6y4mCvcsDIymsPK6d/We9zQ5KsSA1VsbX1miC8/nHQcluzfmL4q', '0', 0, 'KARLA DUARTE', NULL, NULL, 'correo4@gmail.com', NULL, '98546254', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-08-31 13:11:19', '2020-08-31 13:11:55', NULL, NULL, NULL, 133, 133, NULL),
(404, 'CORREO5@GMAIL.COM', '$2a$10$T3Rd6PnmW4m1yqes.ZEUmujBIqTXoVWPECDgsiIuxK9YxCQRInZ6y', '0', 0, 'STEVEN APONTE', NULL, NULL, 'CORREO5@GMAIL.COM', NULL, '62356899', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-08-31 13:16:32', '2020-08-31 13:17:57', NULL, NULL, NULL, 403, 403, NULL),
(405, 'CORREO6@GMAIL.COM', '$2a$10$fycYXPfL0lyblG74WGF3IuGqaNYsIoW0Wcg3jEmwcG6dDPA238hN.', '0', 0, 'CAMILO TORRES', NULL, NULL, 'CORREO6@GMAIL.COM', NULL, '62565489', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2020-09-01 14:53:02', '2020-09-01 15:03:54', NULL, NULL, NULL, 344, 344, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `typeid` (`typeid`);

--
-- Indices de la tabla `contents_type`
--
ALTER TABLE `contents_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indices de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indices de la tabla `gallery_category`
--
ALTER TABLE `gallery_category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `member_log`
--
ALTER TABLE `member_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `purchase_products`
--
ALTER TABLE `purchase_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`(255)),
  ADD UNIQUE KEY `username` (`username`(255)),
  ADD UNIQUE KEY `face` (`facebookId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `contents_type`
--
ALTER TABLE `contents_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `gallery_category`
--
ALTER TABLE `gallery_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234262;
--
-- AUTO_INCREMENT de la tabla `purchase_products`
--
ALTER TABLE `purchase_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=406;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contents`
--
ALTER TABLE `contents`
  ADD CONSTRAINT `typeid` FOREIGN KEY (`typeid`) REFERENCES `contents_type` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `gallery_category` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `purchase_products`
--
ALTER TABLE `purchase_products`
  ADD CONSTRAINT `purchase_products_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
