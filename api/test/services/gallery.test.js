const app = require('../../src/app');

describe('\'gallery\' service', () => {
  it('registered the service', () => {
    const service = app.service('gallery');
    expect(service).toBeTruthy();
  });
});
