// Initializes the `faqs` service on path `/faqs`
const { Faqs } = require('./faqs.class');
const createModel = require('../../models/faqs.model');
const hooks = require('./faqs.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/faqs', new Faqs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('faqs');

  service.hooks(hooks);
};
