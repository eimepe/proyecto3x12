// Initializes the `loginfacebook` service on path `/loginfacebook`
const { Loginfacebook } = require('./loginfacebook.class');
const hooks = require('./loginfacebook.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/loginfacebook', new Loginfacebook(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('loginfacebook');

  service.hooks(hooks);
};
