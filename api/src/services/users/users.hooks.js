const { authenticate } = require("@feathersjs/authentication").hooks;

const {
  hashPassword,
  protect,
} = require("@feathersjs/authentication-local").hooks;

const usermatri = require("../../hooks/usermatri");

const mailregister = require("../../hooks/mailregister");

const matriupdate = require("../../hooks/matriupdate");

const selectultimo = require("../../hooks/selectultimo");

const imgUser = require("../../hooks/img-user");

module.exports = {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [authenticate("jwt")],
    create: [hashPassword("password")],
    update: [hashPassword("password"), authenticate("jwt")],
    patch: [hashPassword("password"), imgUser()],
    remove: [authenticate("jwt")],
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect("password"),
    ],
    find: [selectultimo(), usermatri()],
    get: [usermatri()],
    create: [mailregister()],
    update: [matriupdate()],
    patch: [matriupdate()],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
