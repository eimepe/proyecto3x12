

const imgNews = require('../../hooks/img-news');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [imgNews()],
    update: [imgNews()],
    patch: [imgNews()],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
