// Initializes the `contactenos` service on path `/contactenos`
const { Contactenos } = require('./contactenos.class');
const hooks = require('./contactenos.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contactenos', new Contactenos(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('contactenos');

  service.hooks(hooks);
};
