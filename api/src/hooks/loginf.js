// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const { authenticate } = require('@feathersjs/authentication');
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, method, result, params } = context;

    //console.log(context);
    var user =  await context.app.service('users').find({
      query:{
        facebookid: params.query.facebookid
      }
     });
     var userdata = user.data[0];
     userdata.password = null;
     
  
     if(user.total>0){
      var r = await app.service('/authentication').create({"strategy": "face",
      "user":userdata });
      context.result= r;
     }else{
      context.result = {response: "error"}
     }
     
 
    
   

    return context;
  };
};
