// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars

module.exports = function (options = {}) {
  return async (context) => {
    const { app, method, result, params } = context;
    const categories = method === "find" ? result.data : [result];
    let contador = 0;
    if (params.query.parent_id) {
      await Promise.all(
        categories.map(async (subcategory) => {
          const subcategorysub = await app.service("member-log").find({
            query: {
              parent_id: subcategory.id,
            },
          });
          const user = await app.service("users").get(subcategory.id);

          subcategory.image =
            "http://3.21.152.189/mlm/uploads/profiles/" + user.picture_url;
          subcategory.title = user.mobile_no;
          if (subcategorysub.data.length > 0) {
            contador = contador + 1;
          }
          subcategory.children = subcategorysub.data;
        })
      );
    } else {
      await Promise.all(
        categories.map(async (subcategory) => {
          if (subcategory.parent_id > 0) {
            const subcategorysub = await app
              .service("member-log")
              .get(subcategory.parent_id);
            console.log(subcategorysub.name);

            subcategory.parent = subcategorysub.name;
          } else {
            subcategory.parent = "No Aplica";
          }
        })
      );
    }
    /* if(params.query.parent_id==0){
      await Promise.all(categories.map(async category => {
 
        const subcatagories = await app.service('categories').find({
          query: {
            parent_id: category.id
          }
        });

     category.subcate = subcatagories.data;
     

     await Promise.all(subcatagories.data.map(async subcategory => {
        const subcategorysub = await app.service('categories').find({
          query: {
            parent_id: subcategory.id
          }
        });

      subcategory.sub = subcategorysub.data;
     }));
   
    
    }));
    }*/
    context.result.contador = contador;
    return context;
  };
};
