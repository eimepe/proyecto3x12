// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { app, method, result, params } = context;
    const userr = method === "find" ? result.data[0] : result;
    var contador = 0;
    if (typeof userr === "undefined") {
      // your code here
      console.log("error al iniciar sesion");
    } else {
      if (userr.Matriculador == 0) {
        userr.matri = {};
      } else if (userr.Matriculador == null) {
        userr.matri = {};
      } else {
        console.log("Buscando matriculador" + userr.Matriculador);
        if (contador == 0) {
          contador = contador + 1;
          console.log(contador);
          const user = await context.app
            .service("users")
            .get(userr.Matriculador);

          userr.matri = user;
        }
      }

      if (userr.Patrocinador == 0) {
        userr.patri = {};
      } else if (userr.Patrocinador == null) {
        userr.patri = {};
      } else {
        const userp = await context.app
          .service("users")
          .get(userr.Patrocinador);
        console.log(userp);
        userr.patri = userp;
      }
    }

    return context;
  };
};
