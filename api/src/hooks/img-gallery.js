// eslint-disable-next-line no-unused-vars

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
var fs = require("fs");

module.exports = function (options = {}) {
  // eslint-disable-line no-unused-vars
  return async (context) => {
    const { data } = context;

    var imgfile = context.data.name;

    if (imgfile) {
      console.log(imgfile);
      // The actual message text
      var images = null;

      var imgbase = imgfile.split(",");

      var imgvalidate = imgfile.split(".");

      if (
        imgvalidate[1] == "jpg" ||
        imgvalidate[1] == "png" ||
        imgvalidate[1] == "jpeg"
      ) {
        var img = imgfile;
      } else {
        var base64Data = imgbase[1];

        var nombreimg = new Date().getTime() + "gallery.png";

        // grabas la imagen el disco
        fs.writeFile(
          "./public/images/gallery/" + nombreimg,
          base64Data,
          "base64",
          function (err) {
            console.log(err);
          }
        );

        images = nombreimg;
      }

      context.data = {
        category_id: context.data.category_id,
        name: images,
        description: context.data.description,
        create_date: new Date().getTime(),
        createdAt: new Date().getTime(),
        updatedAt: new Date().getTime(),
      };
    }

    // Best practice: hooks should always return the context
    return context;
  };
};
