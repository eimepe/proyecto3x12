// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { app, method, result, params } = context;
    const categories = method === "find" ? result.data : [result];

    if (params.query.typeid) {
      await Promise.all(
        categories.map(async (subcategory) => {
          const subcategorysub = await app.service("contents").find({
            query: {
              parentid: subcategory.id,
            },
          });
          /* const user = await app.service("users").get(subcategory.id);

          subcategory.image =
            "http://3.21.152.189/mlm/uploads/profiles/" + user.picture_url;
          subcategory.title = user.mobile_no;*/
          subcategory.parent = subcategorysub.data;
        })
      );
    } else {
      await Promise.all(
        categories.map(async (subcategory) => {
          if (subcategory.parent_id > 0) {
            const subcategorysub = await app
              .service("member-log")
              .get(subcategory.parent_id);
            console.log(subcategorysub.name);

            subcategory.parent = subcategorysub.name;
          } else {
            subcategory.parent = "No Aplica";
          }
        })
      );
    }
    return context;
  };
};
