// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    const { app, method, result, params } = context;
    const categories = method === "find" ? result.data : [result];

    await Promise.all(
      categories.map(async (galeria) => {
        const gallery = await app.service("gallery").find({
          query: {
            category_id: galeria.id,
          },
        });
        galeria.children = gallery.data;
      })
    );

    return context;
  };
};
