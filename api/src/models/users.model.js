// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get("sequelizeClient");
  const users = sequelizeClient.define(
    "users",
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      first_name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      alternateEmail: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      language: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      profile_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      dob: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      about: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      locale: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      designation: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      country: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      city: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      pincode: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      vat_number: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      AccountNumber: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      IFSCCode: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      ip_address: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      created: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      iccid: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      imei: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      portabilidad: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      numero: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      operador_donante: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      tipo_identificacion: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      nacionalidad: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      ciudad: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      corregimiento: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      barriada: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      calle: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      casa: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      ph: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      apartamento: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      aclaracion: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      numcelularllamadas: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      mobile_no: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      numcelularwhatsapp: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      numotro: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      banco: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      numerocuenta: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      ifpago: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: 0,
      },
      ifpago_fecha: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      lastlogged: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      modified: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      picture_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      Matriculador: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      Patrocinador: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },

      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      reset: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      datereset: {
        type: DataTypes.STRING,
        allowNull: true,
      },

      facebookId: { type: DataTypes.STRING, unique: true },
      numidentificacion: {
        type: DataTypes.INTEGER(20),
        allowNull: true,
      },
    },

    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line no-unused-vars
  users.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return users;
};
