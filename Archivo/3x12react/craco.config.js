const CracoLessPlugin = require("craco-less");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": "#f9004d",
              "@border-radius-base": "10px",
              "@link-color": "#1d1d24",
              "@btn-danger-bg": "#4267b2",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
