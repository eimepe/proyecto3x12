import React from 'react';
import { Layout, Menu, Breadcrumb, Button, Row, Col } from "antd";

import fondo from "../../static/images/bg/bg-image-1.jpg";

const Subtitle = (props) =>{

  if(props.imagenfondo){
    var imgfondo  =props.imagenfondo;
  }else{
    var imgfondo  =fondo;
  }
    return (
      
      <div
          className="breadcrumb-area rn-bg-color ptb--120 bg_image bg_image--1 pad-m"
          style={{ backgroundImage: "url(" + `${imgfondo}`+ ")" }}
          data-black-overlay="6">
            <Row>
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <div className="breadcrumb-inner pt--100 pt_sm--40 pt_md--50">
                  <h2 className="title">{props.subtitle}</h2>
                  <ul className="page-list">
                    <li>
                      <a href="/">Inicio</a>
                    </li>
                    <li className="current-page">{props.subtitle}</li>
                  </ul>
                </div>
              </Col>
            </Row>
      </div>
    )
}
export default Subtitle;