import React from "react";
import "../../static/css/style.css";
import { Layout, Row, Col, Carousel } from "antd";
import Constants from "../../utils/Constants";

const { Header, Content, Footer, Sider } = Layout;

var urlB = `${Constants.URLFILES}/mlm/uploads/content/banner_116.jpg`;

class Banner extends React.Component {
  state = {
    Databanners: [],
  };

  GetBanners = (values) => {
    console.log(values);

    fetch(`${Constants.URL}/banners?status=1`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ Databanners: res.data }));
  };

  
  mostrarBanner = () => {
    const banner = this.state.Databanners;
    return (
      <React.Fragment>
        <Carousel autoplay>
          {banner.map((ban) => (
            <Row justify="center" className="text-center">
              <Col span={24} className="slider-box-content"  style={{ backgroundImage: "url(" + `${Constants.URL}/images/banners/`+ban.photo + ")" }}>
                <div className={"inner spacinh text-"+ban.text_align}>
                  <h1 className="title">{ban.title}</h1>
                  <p className="description">{ban.description}</p>
                  <div className="slide-btn">
                    <a
                      className="rn-button-style--2 btn-solid"
                      href={ban.button_link}
                    >
                      {ban.button_text}
                    </a>
                  </div>
                </div>
              </Col>
            </Row>
          ))}
        </Carousel>
      </React.Fragment>
    );
  };

  componentDidMount() {
    this.GetBanners();
  }

  render() {
    return <React.Fragment>{this.mostrarBanner()}</React.Fragment>;
  }
}
export default Banner;
// ReactDOM.render(<DemoCarousel />, document.querySelector('.demo-carousel'));

// Don't forget to include the css in your page
// <link rel="stylesheet" href="carousel.css"/>
// Begin DemoSliderControls
