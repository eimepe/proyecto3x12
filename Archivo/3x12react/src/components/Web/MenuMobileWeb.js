import React, { useState } from "react";
import { Drawer, Button, Menu } from "antd";
import "../../static/css/menumobile.css";
import { MenuOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Constants from "../../utils/Constants";

let { SubMenu } = Menu;

class MenuMobileWeb extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: false,
      DatamenuMobile: [],
    };
  }

  showDrawer = () => {
    this.setState({ visible: true });
  };

  onClose = () => {
    this.setState({ visible: false });
  };

  GetMenusMobile = (values) => {
    console.log(values);
    fetch(`${Constants.URL}/contents?status=1&typeid=1`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res.data);
        this.setState({ DatamenuMobile: res.data });
      });
  };

  urlAmigable = (url) => {
    return url
      .toString()
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .replace(/\s+/g, "-")
      .toLowerCase()
      .replace(/&/g, "-and-")
      .replace(/[^a-z0-9\-]/g, "")
      .replace(/-+/g, "-")
      .replace(/^-*/, "")
      .replace(/-*$/, "");
  };

  componentDidMount() {
    this.GetMenusMobile();
    console.log(this.state.DatamenuMobile);
  }

  render() {
    const menu = this.state.DatamenuMobile;

    return (
      <div className="menumobile">
        <Button type="primary" onClick={this.showDrawer}>
          <MenuOutlined />
        </Button>
        <Drawer
          title="Menu"
          placement="right"
          closable={true}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <Menu theme="light" mode="vertical">
            <Menu.Item key="0">
              <Link to="/">Inicio</Link>
            </Menu.Item>

            {menu.map((men, index) => (
              <Menu.Item key={index}>
                <Link to={"/" + men.menu}>{men.menu}</Link>
              </Menu.Item>
            ))}

            <Menu.Item key="4">
              <Link to="/contactenos">Contactenos</Link>
            </Menu.Item>

            <Menu.Item key="5">
              <Link to="/faqs">Faqs</Link>
            </Menu.Item>
            <Menu.Item key="6">
              <Link to="/noticias">Noticias</Link>
            </Menu.Item>

            <Menu.Item key="7">
              <Link to="/galeria">Galeria</Link>
            </Menu.Item>
            {this.props.user.accessToken ? (
              <Menu.Item key="7">
                <Link to="/micuenta">Hola( {this.props.user.user.name})</Link>
              </Menu.Item>
            ) : (
              <Menu.Item key="8">
                <Link to="/login">Inicia Sesión</Link>
              </Menu.Item>
            )}

            {this.props.user.accessToken ? null : (
              <Button type="primary" className="btn-afiliate">
                <Link to="/register"> Afíliate AHORA!</Link>
              </Button>
            )}
          </Menu>
        </Drawer>
      </div>
    );
  }
}

const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(MenuMobileWeb);
