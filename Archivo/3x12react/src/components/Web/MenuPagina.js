import React, { useState } from "react";
import { Layout, Menu, Breadcrumb, Button, Sider, Drawer } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Constants from "../../utils/Constants";
import {
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from "@ant-design/icons";
let { SubMenu } = Menu;

class MenuPagina extends React.Component {
  scroll = (pos) => {
    const elemento = document.querySelector(pos);
    elemento.scrollIntoView("smooth", "end");
  };

  state = {
    Datamenu: [],
  };

  GetMenus = (values) => {
    console.log(values);
    fetch(`${Constants.URL}/contents?status=1&typeid=1`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ Datamenu: res.data }));
  };

  urlAmigable = (url) => {
    return url
      .toString()
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .replace(/\s+/g, "-")
      .toLowerCase()
      .replace(/&/g, "-and-")
      .replace(/[^a-z0-9\-]/g, "")
      .replace(/-+/g, "-")
      .replace(/^-*/, "")
      .replace(/-*$/, "");
  };

  mostrarMenu = () => {
    const menu = this.state.Datamenu;
    return (
      <>
        <Menu
          theme="light"
          mode="horizontal"
          className="menu-principal menu-escritorio"
        >
          <Menu.Item key="0">
            <Link to="/">Inicio</Link>
          </Menu.Item>

          {menu.map((men) => {
            return (
              <SubMenu
                key={men.id}
                title={
                  <Link
                    to={
                      "/contenido/" + this.urlAmigable(men.menu) + "-" + men.id
                    }
                  >
                    {men.menu}
                  </Link>
                }
              >
                {men.parent.map((sub) => {
                  return (
                    <Menu.Item key={sub.id}>
                      <Link
                        to={
                          "/contenido/" +
                          this.urlAmigable(sub.menu) +
                          "-" +
                          sub.id
                        }
                      >
                        {sub.menu}
                      </Link>
                    </Menu.Item>
                  );
                })}
              </SubMenu>
            );
          })}

          <Menu.Item key="4">
            <Link to="/contactenos">Contactenos</Link>
          </Menu.Item>

          <Menu.Item key="5">
            <Link to="/faqs">Faqs</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="/noticias">Noticias</Link>
          </Menu.Item>

          <Menu.Item key="7">
            <Link to="/galeria">Galeria</Link>
          </Menu.Item>

          {this.props.user.accessToken ? (
            <Menu.Item key="8">
              <Link to="/micuenta">{this.props.user.user.name}</Link>
            </Menu.Item>
          ) : (
            <Menu.Item key="9">
              <Link to="/login">Inicia Sesión</Link>
            </Menu.Item>
          )}

          {this.props.user.accessToken ? null : (
            <Button type="primary" className="btn-afiliate">
              <Link to="/register"> Afíliate AHORA!</Link>
            </Button>
          )}
        </Menu>
      </>
    );
  };

  componentDidMount() {
    this.GetMenus();
  }

  render() {
    return <React.Fragment>{this.mostrarMenu()}</React.Fragment>;
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(MenuPagina);
