import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import { Layout, Space, Menu, Breadcrumb, Button, Drawer } from "antd";
import {
  UserOutlined,
  LaptopOutlined,
  SettingOutlined,
  MenuOutlined,
} from "@ant-design/icons";
import { connect } from "react-redux";
import Layoutpagina from "./LayoutPagina";
import { Link, Redirect } from "react-router-dom";
import { setUser } from "../../redux/actions";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class Layoutcuenta extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: "0",
      visible: false,
    };
  }
  handleClick = (e) => {
    console.log("click ", e.key);
    var c = this;
    setTimeout(() => {
      c.setState({
        current: e.key,
      });
    }, 1000);
  };

  showDrawer() {
    this.setState({ visible: true });
  }
  onClose() {
    this.setState({ visible: false });
  }

  render() {
    if (!this.props.user.accessToken) {
      return <Redirect to="/" />;
    } else {
      return (
        <Layoutpagina>
          <Layout
            className="layout-cuenta"
            style={{
              marginTop: "150px",
              flexDirection: "row",
              justifyContent: "start",
            }}
          >
            <Sider
              className="site-layout-background menu-escritorio"
              width={200}
            >
              <Menu
                mode="inline"
                defaultSelectedKeys={[this.state.current]}
                defaultOpenKeys={["sub1"]}
                style={{ height: "100%" }}
                onClick={this.handleClick.bind(this)}
              >
                <SubMenu key="sub1" icon={<UserOutlined />} title="Mi cuenta">
                  <Menu.Item key="1">
                    <Link to="/micuenta">Mis Datos</Link>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Link to="/piramidal">Mi equipo</Link>
                  </Menu.Item>
                  <Menu.Item key="3">
                    <Link to="/estructuralineal">Estructura lineal</Link>
                  </Menu.Item>
                  <Menu.Item key="4" onClick={() => this.props.setUser({})}>
                    Cerar Sesion
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>

            <Button
              style={{ maxWidth: "50px", height: 50, margin: 20 }}
              type="primary"
              className="menumobile"
              onClick={this.showDrawer.bind(this)}
            >
              <MenuOutlined />
            </Button>
            <Drawer
              title="Mi cuenta"
              placement="left"
              closable={true}
              onClose={this.onClose.bind(this)}
              visible={this.state.visible}
            >
              <Menu theme="light" mode="vertical">
                {/* <MenuMobile/> */}

                <Menu.Item key="1">
                  <Link to="/micuenta">Mis Datos</Link>
                </Menu.Item>
                <Menu.Item key="2">
                  <Link to="/piramidal">Mi equipo</Link>
                </Menu.Item>
                <Menu.Item key="3">
                  <Link to="/estructuralineal">structura lineal</Link>
                </Menu.Item>
                <Menu.Item key="4" onClick={() => this.props.setUser({})}>
                  Cerar sesion
                </Menu.Item>
              </Menu>
            </Drawer>

            <Layout
              style={{ padding: "0 24px 24px" }}
              className="layout-cuenta-data"
            >
              <Content
                className="site-layout-background layout-cuenta-data"
                style={{
                  padding: 24,
                  marginLeft: 20,
                  marginRight: 20,
                  minHeight: 280,
                }}
              >
                {this.props.children}
              </Content>
            </Layout>
          </Layout>
        </Layoutpagina>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Layoutcuenta);
