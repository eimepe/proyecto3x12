import React, { useState } from "react";
import { Layout, Menu, Breadcrumb, Button, Sider, Drawer } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { MenuOutlined } from "@ant-design/icons";

class Menuprincipal extends React.Component {
  scroll = (pos) => {
    const elemento = document.querySelector(pos);

    elemento.scrollIntoView("smooth", "end");
  };

  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <Menu
        theme="light"
        mode="horizontal"
        className="menu-principal menu-escritorio"
      >
        <Menu.Item key="1">
          <Link to="/landing" onClick={() => this.scroll("#inicio")}>
            Inicio
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/landing" onClick={() => this.scroll("#nosotros")}>
            Nosotros
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/landing" onClick={() => this.scroll("#oportunidades")}>
            Oportunidades
          </Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/landing" onClick={() => this.scroll("#contactenos")}>
            Contactenos
          </Link>
        </Menu.Item>

        {this.props.user.accessToken ? (
          <Menu.Item key="5">
            <Link to="/micuenta">Hola( {this.props.user.user.name})</Link>
          </Menu.Item>
        ) : (
          <Menu.Item key="6">
            <Link to="/login">Inicia Sesión</Link>
          </Menu.Item>
        )}

        {this.props.user.accessToken ? null : (
          <Button type="primary" className="btn-afiliate">
            <Link to="/register"> Afíliate AHORA!</Link>
          </Button>
        )}
      </Menu>
    );
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(Menuprincipal);
