import React from "react";
import { Layout, Menu, Breadcrumb, Button,Row,Col } from "antd";
import { Link } from "react-router-dom";


const { Footer } = Layout;


class FooterLanding extends React.Component {
  render() {
    return <Footer>
      <Row justify="center" className="text-center" >
        <Col>
         <span class="text_rojo"> 3x12 panama 2020</span>
        </Col>
      </Row>
    </Footer>;
  }
}
export default FooterLanding;
