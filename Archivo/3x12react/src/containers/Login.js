import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/registro.css";
import { connect } from "react-redux";
import { setUser } from "../redux/actions";
import {
  Layout,
  Typography,
  Divider,
  Button,
  Form,
  Input,
  Modal,
  Row,
  Col,
} from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import { Redirect } from "react-router-dom";
import LayoutInicial from "../components/Layout/LayoutInicial";
import Constants from "../utils/Constants";
import MetaTags from "react-meta-tags";

import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import Subtitle from "../components/Web/Subtitle";
const { Header, Content, Footer, Sider } = Layout;
const { Title, Text } = Typography;
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      response: "",
      modalreset: false,
      email: null,
      status: null,
    };
    this.iniciarsesionf = this.iniciarsesionf.bind(this);
    this.responseFacebook = this.responseFacebook.bind(this);
  }

  iniciarsesionf(faceid) {
    fetch(Constants.URL + "/loginfacebook?facebookid=" + faceid, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.accessToken) {
          this.props.setUser(res);
          console.warn(res);
          this.props.navigation.navigate("Home");
        } else {
          console.warn("error de autenticacion");
          this.setState({
            visibleok: true,
            contentmodalok:
              "No estas registrado con facebook en nuetra plataforma",
          });
          Modal.error({
            content:
              "No estas registrado con facebook en nuetra plataforma, por favor registrate",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  responseFacebook(response) {
    console.log(response);
    this.iniciarsesionf(response.id);
  }

  resetPass() {
    this.setState({ modalreset: true });
  }

  reset() {
    console.log(Constants.URL);
    fetch(Constants.URL + "/resetpass", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: this.state.email,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res.response);
        if (res.response == "1") {
          this.setState({ status: 1 });
        } else {
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  Login = (values) => {
    console.log(values);
    var data = {
      email: values.email,
      password: values.password,
      strategy: "local",
    };

    fetch(`${Constants.URL}/authentication`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.code == 401) {
          Modal.error({
            content: "Error de usuario o contraseña",
          });
        } else if (res.code == 500) {
          Modal.error({
            content: "Error de usuario o contraseña",
          });
        } else if (res.accessToken) {
          this.props.setUser(res);
        }
        this.setState({ visible: true });
      });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  onChange = ({ target: { value } }) => {
    this.setState({ email: value });
  };
  render() {
    const layout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 15 },
    };
    const tailLayout = {
      wrapperCol: { span: 25 },
    };

    if (this.props.user.accessToken) {
      return <Redirect to="/micuenta" />;
    } else if (this.state.status == 1) {
      return <Redirect to="/resetpass" />;
    } else {
      return (
        <LayoutPagina>
          <MetaTags>
            <title>Inicio de sesión</title>
            <meta
              name="description"
              content="inicio de sesion plataforma 3x12."
            />
            <meta property="og:title" content="plataforma 3 x12" />
          </MetaTags>
          <Subtitle subtitle="Inicio de sesión" />
          <Content>
            <Row
              justify="center"
              align="middle"
              className=" contenido-register text-center"
              gutter={[48, 24]}
            >
              <Col span={24}>
                <Title>Iniciar sesión</Title>
              </Col>
              <Col span={18} flex="1">
                <Form onFinish={this.Login} layout="horizontal" {...layout}>
                  <Form.Item
                    rules={[
                      { required: true, message: "Este campo es obligatorio" },
                    ]}
                    name="email"
                    label="Email"
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    rules={[
                      { required: true, message: "Este campo es obligatorio" },
                    ]}
                    name="password"
                    label="Contraseña"
                    hasFeedback
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                      Iniciar sesión
                    </Button>
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button type="link" onClick={this.resetPass.bind(this)}>
                      Olvidaste tu contraseña?
                    </Button>
                  </Form.Item>

                  <FacebookLogin
                    appId="3431636803525474"
                    autoLoad={false}
                    callback={this.responseFacebook}
                    render={(renderProps) => (
                      <Form.Item {...tailLayout}>
                        <Button type="danger" onClick={renderProps.onClick}>
                          Iniciar con facebook
                        </Button>
                      </Form.Item>
                    )}
                  />
                </Form>
              </Col>
            </Row>
          </Content>
          <Modal
            title="Recuperar Contraseña"
            visible={this.state.modalreset}
            onOk={this.reset.bind(this)}
            onCancel={() => this.setState({ modalreset: false })}
          >
            <Text>Escriba su correo electronico para recuperar contraseña</Text>
            <Input
              value={this.state.email}
              onChange={this.onChange}
              name="email"
              placeholder={"Correo electronico"}
            />
          </Modal>
        </LayoutPagina>
      );
    }
  }
}

const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser: setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Login);
