import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/style.css";
import { Layout, Row, Col ,Form,Input,Button,Modal,Collapse} from "antd";

import Subtitle from "../components/Web/Subtitle";
import LayoutPagina from "../components/Layout/LayoutPagina"
import MetaTags from "react-meta-tags";
import Constants from "../utils/Constants";
import { connect } from "react-redux";


const { Panel } = Collapse;

const { Header, Content, Footer, Sider } = Layout;



class Faqs extends React.Component {

  state = {
    DataFaqs: [],
  };
  GetFaqs = (values) => {
    console.log(values);

    fetch(`${Constants.URL}/faqs?$limit=40`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ DataFaqs: res.data }));
  };
  componentDidMount() {
    this.GetFaqs();
  }
  
  render() {
    const faqs = this.state.DataFaqs;
    
    return (
        <LayoutPagina>
          <MetaTags>
            <title>{this.props.diccionario.faqs} | 3x12</title>
            <meta
              name="description"
              content="Preguntas frecuentes 3x12 Plataforma "
            />
            <meta property="og:title" content="Preguntas frecuentes plataforma 3x12" />
          </MetaTags>
          <Subtitle subtitle={this.props.diccionario.faqs}/>


          <Row   justify="left" className="mt_dec--40 text-center contenido-register" gutter={[48, 8]}> 
            <Col xs={24} sm={24} md={12} lg={12} xl={12} className=" mt--40">  
            {/* <h2 class="title">Preguntas Frecuentes 3x12</h2> */}
            <Collapse accordion>
             {faqs.map((faq,index) => ( 
              
              <Panel header={faq.question} key={index}>
                <p>{faq.answer}</p>
              </Panel>
              

             ))} 
             
            </Collapse>
                        

            </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12} className=" mt--40">  
              <div class="thumbnail mb_md--40 mb_sm--40">
                <img src="http://3.19.39.85/mlm/public/web/assets/images/about/about-6.jpg" alt="trydo" />
              </div>
          </Col>
          </Row>
          
          
        </LayoutPagina>
        )
  }
}
const mapStateProps = (state) => {
  return {
    diccionario: state.diccionario,
  };
};

const mapDispatchToProps = {
  //setDiccionario: setDiccionario,
};
export default connect(mapStateProps, mapDispatchToProps)(Faqs);
