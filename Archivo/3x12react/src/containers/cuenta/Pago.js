import React from "react";

import {
  Input,
  Layout,
  Row,
  Col,
  Menu,
  Typography,
  Button,
  Form,
  Modal,
} from "antd";
import OrganizationChart from "@dabeng/react-orgchart";
import visa from "../../static/images/visa.png";
import master from "../../static/images/master.png";
import { UserOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import "../../static/css/pay.css";
import Layoutcuenta from "../../components/Layout/Layoutcuenta";
import { setUser } from "../../redux/actions";
import { Redirect } from "react-router-dom";
import Constants from "../../utils/Constants";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const { Title, Text } = Typography;

class Pago extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clase: "ocultar",
      ds: {},
      dss: {},
      type: "NA",
      redirect: false,
    };
    this.getdata = this.getdata.bind(this);
  }

  abrir() {}

  getdata() {}

  checkPrice = (rule, number) => {
    this.setState({ numbercard: number });
    if (number.match(/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/)) {
      var visa_error = "Es Visa";
      this.setState({ type: "VISA" });
      return Promise.resolve();

      console.log(visa_error);
    } else if (number.match(/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/)) {
      console.log("Es marter card");
      this.setState({ type: "MASTERCARD" });
      return Promise.resolve();
    } else {
      console.log("No valido");
      this.setState({ type: "NV" });
      return Promise.reject("Numero de tarjeta no valido");
    }

    return Promise.reject("Numero de tarjeta no valido");
  };

  componentDidMount() {
    this.getdata();
  }

  onFinish = (values) => {
    var modal = Modal.info({
      title: "Procesando",
      content: <div>Estamos procesando su pago por favor espere</div>,
    });

    console.log(values);

    let payload = {
      cclw:
        "D17B05A095489D1176560B4666A283454185F353F401D0201CC5C16F92535DF6B1DEBA18E79442CC0D6F75FD024207680AFBDFD6CF015478BF30CBEF9160A08D",
      amount: 15, //El monto o valor total de la transacción a realizar. NO PONER
      taxAmount: 0,
      email: values.email, //String MaxLength:100 Email del
      phone: values.phone, //Numeric MaxLength:16 Teléfono del Tarjeta habiente,
      address: values.direccion, //String MaxLength:100 Dirección del Tarjeta,
      concept: "Pago inicial", //MaxLength:150 ;Es la descripción o el motivo de la transacción en proceso
      description: "Pago inicial Shid", //MaxLength:150 ;Es la descripción o el motivo de la transacción en proceso
      lang: "ES", //EN

      cardInformation: {
        cardNumber: values.cardnumber,
        expMonth: values.mes, //Mes de expiración de la tarjeta, siempre 2 dígitos
        expYear: values.ano, //Numeric Ej.:02 Año de expiración de la tarjeta.
        cvv: values.cvv, //Código de Seguridad de la tarjeta Numeric MaxLength:3
        firstName: values.nombres, //String MaxLength:25 Nombre del tarjeta habiente
        lastName: values.apellidos, //String MaxLength:25 Apellido del Tarjeta habiente
        cardType: this.state.type,
      },
    };
    fetch("https://sandbox.paguelofacil.com/rest/processTx/AUTH_CAPTURE", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        authorization:
          "WT5hTaUcpa4J3h4AmrZa2EXXJs8boUVa|DIRd852djHbq2j5Fca5VDUkDbExTBCVf",
      },
      body: JSON.stringify(payload),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.data.status == 0) {
          modal.destroy();
          Modal.error({
            content:
              "Error al procesar su pago verifique los datso e intente de nuevo",
          });
        } else if (res.data.status == 1) {
          let c = this;
          modal.destroy();
          c.changesStatususer();
        }
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  changesStatususer() {
    fetch(`${Constants.URL}/users/` + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ifpago: 1,
        ifpago_fecha: new Date(),
        status: 3,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.setState({ redirect: true });
          this.props.setUser(users);
          Modal.success({
            content: "Su pago fue recibido correctamente",
          });
        } else {
          console.warn("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/micuenta" />;
    } else {
      return (
        <Layoutcuenta context={this.props.history}>
          <Row>
            <Col span="5"></Col>
            <Col span="14">
              <Text style={{ fontSize: 18 }}>
                Aqui puedes pagar con tu trajeta de credito visa o master card
              </Text>

              <Text style={{ fontSize: 16 }}>
                Vas a Realizar un pago de USD{" "}
                <span
                  style={{ fontSize: 18, color: "black", fontWeight: "bold" }}
                >
                  15{" "}
                </span>
                como pago inicial para activar tu cuenta y adquirir tu chip
                claro
              </Text>
              <br />
              <br />
              <Form
                name="pagoenlinea"
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
              >
                <Title level={4}>Datos de la tarjeta</Title>
                <Form.Item
                  name="nombre"
                  rules={[
                    { required: true, message: "Este campo es requerido" },
                  ]}
                >
                  <Input addonBefore="Nombre" />
                </Form.Item>

                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message:
                        "Este campo es requerido y de tipo email   valido ",
                      type: "email",
                    },
                  ]}
                >
                  <Input addonBefore="Correo electronico" />
                </Form.Item>

                <Form.Item
                  name="direccion"
                  rules={[
                    { required: true, message: "Este campo es requerido" },
                  ]}
                >
                  <Input addonBefore="Direccion" />
                </Form.Item>

                <Form.Item
                  name="phone"
                  rules={[
                    { required: true, message: "Este campo es requerido" },
                  ]}
                >
                  <Input addonBefore="Telefono" />
                </Form.Item>
                <Title level={4}>Datos de la tarjeta</Title>
                {this.state.type == "VISA" ? (
                  <img width="80" src={visa} />
                ) : this.state.type == "MASTERCARD" ? (
                  <img width="80" src={master} />
                ) : null}
                <Form.Item
                  name="cardnumber"
                  rules={[{ validator: this.checkPrice }]}
                >
                  <Input maxLength={19} addonBefore="Numero de la tarjeta" />
                </Form.Item>

                <Form.Item
                  name="mes"
                  rules={[
                    {
                      required: true,
                      message: "Este campo es requerido y de tipo numerico",
                    },
                  ]}
                >
                  <Input
                    type="number"
                    maxLength={2}
                    minLength={2}
                    addonBefore="MES MM"
                  />
                </Form.Item>

                <Form.Item
                  name="ano"
                  rules={[
                    {
                      required: true,
                      message: "Este campo es requerido y de tipo numerico",
                    },
                  ]}
                >
                  <Input
                    type="number"
                    maxLength={2}
                    minLength={2}
                    addonBefore="Año AA"
                  />
                </Form.Item>

                <Form.Item
                  name="cvv"
                  rules={[
                    {
                      required: true,
                      message: "Este campo es requerido y de tipo numerico",
                    },
                  ]}
                >
                  <Input
                    type="number"
                    maxLength={3}
                    minLength={3}
                    addonBefore="CVV"
                  />
                </Form.Item>

                <Form.Item
                  name="nombres"
                  rules={[
                    { required: true, message: "Este campo es requerido" },
                  ]}
                >
                  <Input addonBefore="Nombres" />
                </Form.Item>

                <Form.Item
                  name="apellidos"
                  rules={[
                    { required: true, message: "Este campo es requerido" },
                  ]}
                >
                  <Input addonBefore="Apellidos" />
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Realizar pago
                  </Button>
                </Form.Item>
              </Form>
            </Col>
            <Col span="5"></Col>
          </Row>
        </Layoutcuenta>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Pago);
