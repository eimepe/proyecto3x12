import React, { Component } from "react";
import Modal from "antd/lib/modal/Modal";
import { connect } from "react-redux";
import { Form, Input, Button } from "antd";
import { setUser } from "../../../redux/actions";
import Constants from "../../../utils/Constants";
class EditDatosresidencia extends Component {
  setEditdata(values) {
    fetch(Constants.URL + "/users/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nacionalidad: values.nacionalidad,
        ciudad: values.ciudad,
        corregimiento: values.corregimiento,
        barriada: values.barriada,
        calle: values.calle,
        casa: values.casa,
        ph: values.ph,
        apartamento: values.apartamento,
        aclaracion: values.aclaracion,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          this.props.cancel();
        } else {
          console.log("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props.status);
    return (
      <Modal
        title="Editar datos de Residencia"
        visible={this.props.status}
        onCancel={this.props.cancel}
        footer={[
          <Button key="back" onClick={this.props.cancel}>
            Cancelar
          </Button>,
        ]}
      >
        <Form name="basic" onFinish={this.setEditdata.bind(this)}>
          <Form.Item
            label="Nacionalidad"
            name="nacionalidad"
            initialValue={this.props.user.user.nacionalidad}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Ciudad"
            name="ciudad"
            initialValue={this.props.user.user.ciudad}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Corregimiento"
            name="corregimiento"
            initialValue={this.props.user.user.corregimiento}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Barriada"
            name="barriada"
            initialValue={this.props.user.user.barriada}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Calle"
            name="calle"
            initialValue={this.props.user.user.calle}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Casa"
            name="casa"
            initialValue={this.props.user.user.casa}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="PH"
            name="ph"
            initialValue={this.props.user.user.ph}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Apartamento"
            name="apartamento"
            initialValue={this.props.user.user.apartamento}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Aclaracion"
            name="aclaracion"
            initialValue={this.props.user.user.aclaracion}
            rules={[{}]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(EditDatosresidencia);
