import React, { Component } from "react";
import Modal from "antd/lib/modal/Modal";
import { connect } from "react-redux";
import { Form, Input, Button } from "antd";
import { setUser } from "../../../redux/actions";
import Constants from "../../../utils/Constants";
class EditDatoscontacto extends Component {
  setEditdata(values) {
    fetch(Constants.URL + "/users/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: values.email,

        numcelularllamadas: values.numcelularllamadas,
        numcelularwhatsapp: values.numcelularwhatsapp,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          this.props.cancel();
        } else {
          console.log("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props.status);
    return (
      <Modal
        title="Editar datos de contacto"
        visible={this.props.status}
        onCancel={this.props.cancel}
        footer={[
          <Button key="back" onClick={this.props.cancel}>
            Cancelar
          </Button>,
        ]}
      >
        <Form name="basic" onFinish={this.setEditdata.bind(this)}>
          <Form.Item
            label="Correo"
            name="email"
            initialValue={this.props.user.user.email}
            rules={[
              {
                required: true,
                type: "email",
                message: "Debe ser un email valido",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Celular llamadas"
            name="numcelularllamadas"
            initialValue={this.props.user.user.numcelularllamadas}
            rules={[{ message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Celular Whatsapp"
            name="numcelularwhatsapp"
            initialValue={this.props.user.user.numcelularwhatsapp}
            rules={[{ message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(EditDatoscontacto);
