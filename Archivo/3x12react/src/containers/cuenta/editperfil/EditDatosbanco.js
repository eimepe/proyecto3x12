import React, { Component } from "react";
import Modal from "antd/lib/modal/Modal";
import { connect } from "react-redux";
import { Form, Input, Button, Select } from "antd";
import { setUser } from "../../../redux/actions";
import Constants from "../../../utils/Constants";
const { Option } = Select;
class EditDatosbanco extends Component {
  setEditdata(values) {
    fetch(Constants.URL + "/users/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        tipo_identificacion: values.tipo,

        numidentificacion: values.numidentificacion,
        banco: values.banco,
        numerocuenta: values.numerocuenta,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          this.props.cancel();
        } else {
          console.log("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props.status);
    return (
      <Modal
        title="Editar datos Bancarios"
        visible={this.props.status}
        onCancel={this.props.cancel}
        footer={[
          <Button key="back" onClick={this.props.cancel}>
            Cancelar
          </Button>,
        ]}
      >
        <Form
          name="basic"
          initialValue={this.props.user.user.tipo_identificacion}
          onFinish={this.setEditdata.bind(this)}
        >
          <Form.Item
            label="Tipo de identificacion"
            name="tipo"
            rules={[{ required: true, message: "No puede estr vacio" }]}
          >
            <Select>
              <Select.Option value="1">Cedula</Select.Option>
              <Select.Option value="2">Pasaporte</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Numero de identificacion"
            name="numidentificacion"
            initialValue={this.props.user.user.numidentificacion}
            rules={[{ required: true, message: "No puede estr vacio" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Banco"
            name="banco"
            initialValue={this.props.user.user.banco}
            rules={[{ required: true, message: "No puede estr vacio" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Numero de cuenta"
            name="numerocuenta"
            initialValue={this.props.user.user.numerocuenta}
            rules={[{ required: true, message: "No puede estr vacio" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(EditDatosbanco);
