import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/style.css";

import { Layout, Row, Col, Form, Input, Button, Modal } from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import Subtitle from "../components/Web/Subtitle";
import MetaTags from "react-meta-tags";
import Constants from "../utils/Constants";
const { Header, Content, Footer, Sider } = Layout;
const { TextArea } = Input;

class Contactenos extends React.Component {
  SendContact = (values) => {
    console.log(values);
    var data = {
      nombre: values.nombres,
      telefono: values.telefono,
      email: values.email,
      asunto: values.asunto,
      mensaje: values.mensaje,
    };

    fetch(`${Constants.URL}/contactenos`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.email) {
          Modal.success({
            content: "Contacto enviado correctamente",
          });
        } else if (res.id > 0) {
          Modal.error({
            content:
              "Ocurrio un error mientras se eviaba el mensaje, por favor intenta nuevamente. ",
          });
        }
      });
  };

  render() {
    return (
      <LayoutPagina>
        <MetaTags>
          <title>Conctactanos | 3x12</title>
          <meta name="description" content="Contactanos 3x12 Plataforma " />
          <meta property="og:title" content="plataforma 3x12" />
        </MetaTags>
        {/* <Subtitle subtitle="Contactanos"/> */}

        <div className=" rn-contact-address-area rn-section-gap bg_color--5">
          <Row justify="center" className="text-center " gutter={[48, 8]}>
            <Col xs={24} sm={24} md={6} lg={6} xl={6} className=" mt--40">
              <div className="rn-address">
                <div className="icon">
                  <i className="fas fa-phone"></i>
                </div>
                <div className="inner">
                  <h4 className="title">Teléfono</h4>
                  <p>
                    <a href="tel:+50760901234">+507 60901234</a>
                  </p>
                </div>
              </div>
            </Col>

            <Col xs={24} sm={24} md={6} lg={6} xl={6} className=" mt--40">
              <div className="rn-address">
                <div className="icon">
                  <i className="fas fa-envelope"></i>
                </div>
                <div className="inner">
                  <h4 className="title">Email</h4>
                  <p>
                    <a href="mailto:info@3x12panama.net">info@3x12panama.net</a>
                  </p>
                </div>
              </div>
            </Col>

            <Col xs={24} sm={24} md={6} lg={6} xl={6} className=" mt--40">
              <div className="rn-address">
                <div className="icon">
                  <i className="fas fa-map-marker-alt"></i>
                </div>
                <div className="inner">
                  <h4 className="title">Dirección</h4>
                  <p>
                    <a href="map:Panamá, Vía España">Panamá, Vía España</a>
                  </p>
                </div>
              </div>
            </Col>
          </Row>
        </div>

        <Row
          justify="center"
          className="mt_dec--40 text-center  contenido-register"
          gutter={[48, 8]}
        >
          <Col xs={24} sm={24} md={12} lg={12} xl={12} className=" mt--40">
            <div class="section-title text-left mb--50 mb_sm--30 mb_md--30">
              <h2 class="title">Contáctanos</h2>
              <Form onFinish={this.SendContact} layout="horizontal">
                <Row justify="center" className="text-center" gutter={[48, 8]}>
                  <Col span={24}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                          message: "Este campo es obligatorio",
                        },
                      ]}
                      name="nombres"
                      hasFeedback
                    >
                      <Input size="large" placeholder="Nombre" />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                          message: "Este campo es obligatorio",
                          type: "email",
                        },
                      ]}
                      name="email"
                      hasFeedback
                    >
                      <Input size="large" placeholder="Email" />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                          message: "Este campo es obligatorio",
                        },
                      ]}
                      name="telefono"
                      hasFeedback
                    >
                      <Input size="large" placeholder="Telefono" />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                          message: "Este campo es obligatorio",
                        },
                      ]}
                      name="asunto"
                      hasFeedback
                    >
                      <Input size="large" placeholder="Asunto" />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                          message: "Este campo es obligatorio",
                        },
                      ]}
                      name="mensaje"
                      hasFeedback
                    >
                      <TextArea rows={4} placeholder="Mensaje" />
                    </Form.Item>
                  </Col>
                </Row>

                <Form.Item>
                  <Button type="primary" size="large" htmlType="submit">
                    Enviar
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12} className=" mt--40">
            <div class="thumbnail mb_md--40 mb_sm--40">
              <img
                src="http://3.19.39.85/mlm/public/web/assets/images/about/about-6.jpg"
                alt="trydo"
              />
            </div>
          </Col>
        </Row>
      </LayoutPagina>
    );
  }
}

export default Contactenos;
