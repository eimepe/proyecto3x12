import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/style.css";
import { Layout, Menu, Breadcrumb, Button } from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import MetaTags from "react-meta-tags";
const { Header, Content, Footer, Sider } = Layout;

class Nosotros extends React.Component {
  render() {
    return (
      <LayoutPagina>
        <MetaTags>
          <title>Nosotros | 3x12</title>
          <meta
            name="description"
            content="Nosotros 3x12 Plataforma "
          />
          <meta property="og:title" content="Nosotros Plataforma 3x12" />
        </MetaTags>
      </LayoutPagina>
    )
  }
}

export default Nosotros;
