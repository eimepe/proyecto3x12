import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/style.css";
import { Layout, Menu, Breadcrumb, Button } from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import MetaTags from "react-meta-tags";
const { Header, Content, Footer, Sider } = Layout;

class Error404 extends React.Component {
  render() {
    return (
        <LayoutPagina>
          <MetaTags>
            <title>Página no existe | 3x12</title>
            <meta
              name="description"
              content="Página no existe 3x12 Plataforma "
            />
            <meta property="og:title" content=" Página no existe plataforma 3x12" />
          </MetaTags>
          Esta pagina no existe
        </LayoutPagina>
      );
  }
}

export default Error404;
