import React from "react";
import OrganizationChart from "@dabeng/react-orgchart";
import MyNode from "../components/MyNode";
import Constants from "../utils/Constants";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clase: "ocultar",
      ds: {},
      dss: {},
    };
    this.getdata = this.getdata.bind(this);
  }

  abrir() {}

  getdata(id, title, name, image, status) {
    console.log(id);
    if (id > 0 && id != null) {
      fetch(`${Constants.URL}/estructuras?parent_id=${id}`, {
        method: "GET",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((res) => {
          //console.log(res);
          const array = {
            id: id,
            title: title,
            name: name,
            image: image,
            status: parseInt(status),
            children: res.data,
          };

          this.setState({ ds: array });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  componentDidMount() {
    const id = new URLSearchParams(this.props.location.search);
    console.log("valia");
    console.log();
    this.getdata(
      id.get("iduser"),
      id.get("name"),
      id.get("title"),
      id.get("image"),
      parseInt(id.get("status"))
    );
  }

  render() {
    console.log(this.state.ds);
    return (
      <>
        {this.state.ds.id ? (
          <OrganizationChart
            pan={true}
            zoom={true}
            datasource={this.state.ds}
            chartClass="myChart"
            NodeTemplate={MyNode}
          />
        ) : (
          <div class="loader"></div>
        )}
      </>
    );
  }
}

export default App;
