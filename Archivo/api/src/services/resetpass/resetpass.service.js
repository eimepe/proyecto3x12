// Initializes the `resetpass` service on path `/resetpass`
const { Resetpass } = require('./resetpass.class');
const hooks = require('./resetpass.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/resetpass', new Resetpass(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('resetpass');

  service.hooks(hooks);
};
