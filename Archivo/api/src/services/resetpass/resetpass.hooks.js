

const reset = require('../../hooks/reset');

const codereset = require('../../hooks/codereset');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [reset()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [codereset()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
