// Initializes the `member_log` service on path `/member-log`
const { MemberLog } = require('./member_log.class');
const createModel = require('../../models/member_log.model');
const hooks = require('./member_log.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-log', new MemberLog(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-log');

  service.hooks(hooks);
};
