// Initializes the `estructuras` service on path `/estructuras`
const { Estructuras } = require('./estructuras.class');
const hooks = require('./estructuras.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/estructuras', new Estructuras(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('estructuras');

  service.hooks(hooks);
};
