const users = require("./users/users.service.js");
const memberLog = require("./member_log/member_log.service.js");
const faqs = require("./faqs/faqs.service.js");
const resetpass = require("./resetpass/resetpass.service.js");
const loginfacebook = require("./loginfacebook/loginfacebook.service.js");
const contactenos = require("./contactenos/contactenos.service.js");
const news = require("./news/news.service.js");
const contents = require("./contents/contents.service.js");
const estructuras = require("./estructuras/estructuras.service.js");
const estructural = require("./estructural/estructural.service.js");
const banners = require('./banners/banners.service.js');
const diccionario = require('./diccionario/diccionario.service.js');
const gallery = require('./gallery/gallery.service.js');
const galleryCategory = require('./gallery_category/gallery_category.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(memberLog);
  app.configure(faqs);
  app.configure(resetpass);
  app.configure(loginfacebook);
  app.configure(contactenos);
  app.configure(news);
  app.configure(contents);
  app.configure(estructuras);
  app.configure(estructural);
  app.configure(banners);
  app.configure(diccionario);
  app.configure(gallery);
  app.configure(galleryCategory);
};
