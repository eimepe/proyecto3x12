const { authenticate } = require("@feathersjs/authentication").hooks;

const imgGallery = require('../../hooks/img-gallery');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [imgGallery()],
    update: [imgGallery()],
    patch: [imgGallery()],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
