// Initializes the `gallery_category` service on path `/gallery-category`
const { GalleryCategory } = require('./gallery_category.class');
const createModel = require('../../models/gallery_category.model');
const hooks = require('./gallery_category.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/gallery-category', new GalleryCategory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('gallery-category');

  service.hooks(hooks);
};
