

const parentcontent = require('../../hooks/parentcontent');

const imgContents = require('../../hooks/img-contents');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [imgContents()],
    update: [imgContents()],
    patch: [imgContents()],
    remove: []
  },

  after: {
    all: [],
    find: [parentcontent()],
    get: [parentcontent()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
