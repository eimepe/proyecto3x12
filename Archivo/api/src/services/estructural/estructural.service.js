// Initializes the `estructural` service on path `/estructural`
const { Estructural } = require('./estructural.class');
const hooks = require('./estructural.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/estructural', new Estructural(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('estructural');

  service.hooks(hooks);
};
