// Initializes the `diccionario` service on path `/diccionario`
const { Diccionario } = require('./diccionario.class');
const createModel = require('../../models/diccionario.model');
const hooks = require('./diccionario.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/diccionario', new Diccionario(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('diccionario');

  service.hooks(hooks);
};
