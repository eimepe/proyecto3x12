const imgBanners = require("../../hooks/img-banners");

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [imgBanners()],
    update: [imgBanners()],
    patch: [imgBanners()],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
