// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars

module.exports = function (options = {}) {
  return async (context) => {
    const { app, method, result, params } = context;
    //consulta primer nivel
    const categories = await app.service("member-log").find({
      query: {
        parent_id: params.query.parent_id,
        $sort: {
          createdAt: -1,
        },
      },
    });

    let urlimages = "http://3.19.39.85/mlm/uploads/profiles/";
    if (params.query.parent_id) {
      console.log("entra");
      await Promise.all(
        //nivel2
        categories.data.map(async (subcategory) => {
          const subcategorysub = await app.service("member-log").find({
            query: {
              parent_id: subcategory.id,
              $sort: {
                createdAt: -1,
              },
            },
          });

          await Promise.all(
            //nivel3
            subcategorysub.data.map(async (subcategory2) => {
              console.log("entra1");
              const subcategorysub2 = await app.service("member-log").find({
                query: {
                  parent_id: subcategory2.id,
                  $sort: {
                    createdAt: -1,
                  },
                },
              });
              await Promise.all(
                //nivel4
                subcategorysub2.data.map(async (subcategory3) => {
                  console.log("entra2");
                  const subcategorysub3 = await app.service("member-log").find({
                    query: {
                      parent_id: subcategory3.id,
                      $sort: {
                        createdAt: -1,
                      },
                    },
                  });
                  await Promise.all(
                    //nivel5
                    subcategorysub3.data.map(async (subcategory4) => {
                      console.log("entra3");
                      const subcategorysub4 = await app
                        .service("member-log")
                        .find({
                          query: {
                            parent_id: subcategory4.id,
                            $sort: {
                              createdAt: -1,
                            },
                          },
                        });
                      await Promise.all(
                        //nivel6
                        subcategorysub4.data.map(async (subcategory5) => {
                          console.log("entra3");
                          const subcategorysub5 = await app
                            .service("member-log")
                            .find({
                              query: {
                                parent_id: subcategory5.id,
                                $sort: {
                                  createdAt: -1,
                                },
                              },
                            });
                          await Promise.all(
                            //nivel7
                            subcategorysub5.data.map(async (subcategory6) => {
                              console.log("entra3");
                              const subcategorysub6 = await app
                                .service("member-log")
                                .find({
                                  query: {
                                    parent_id: subcategory6.id,
                                    $sort: {
                                      createdAt: -1,
                                    },
                                  },
                                });
                              await Promise.all(
                                //nivel8
                                subcategorysub6.data.map(
                                  async (subcategory7) => {
                                    console.log("entra3");
                                    const subcategorysub7 = await app
                                      .service("member-log")
                                      .find({
                                        query: {
                                          parent_id: subcategory7.id,
                                          $sort: {
                                            createdAt: -1,
                                          },
                                        },
                                      });
                                    await Promise.all(
                                      //nivel9
                                      subcategorysub7.data.map(
                                        async (subcategory8) => {
                                          console.log("entra3");
                                          const subcategorysub8 = await app
                                            .service("member-log")
                                            .find({
                                              query: {
                                                parent_id: subcategory8.id,
                                                $sort: {
                                                  createdAt: -1,
                                                },
                                              },
                                            });
                                          await Promise.all(
                                            //nivel10
                                            subcategorysub8.data.map(
                                              async (subcategory9) => {
                                                console.log("entra3");
                                                const subcategorysub9 = await app
                                                  .service("member-log")
                                                  .find({
                                                    query: {
                                                      parent_id:
                                                        subcategory9.id,
                                                      $sort: {
                                                        createdAt: -1,
                                                      },
                                                    },
                                                  });
                                                await Promise.all(
                                                  //nivel11
                                                  subcategorysub9.data.map(
                                                    async (subcategory10) => {
                                                      console.log("entra3");
                                                      const subcategorysub10 = await app
                                                        .service("member-log")
                                                        .find({
                                                          query: {
                                                            parent_id:
                                                              subcategory10.id,
                                                            $sort: {
                                                              createdAt: -1,
                                                            },
                                                          },
                                                        });
                                                      await Promise.all(
                                                        //nivel12
                                                        subcategorysub10.data.map(
                                                          async (
                                                            subcategory11
                                                          ) => {
                                                            console.log(
                                                              "entra3"
                                                            );
                                                            const subcategorysub11 = await app
                                                              .service(
                                                                "member-log"
                                                              )
                                                              .find({
                                                                query: {
                                                                  parent_id:
                                                                    subcategory11.id,
                                                                  $sort: {
                                                                    createdAt: -1,
                                                                  },
                                                                },
                                                              });
                                                            const user = await app
                                                              .service("users")
                                                              .get(
                                                                subcategory11.id
                                                              );
                                                            subcategory11.image =
                                                              urlimages +
                                                              user.picture_url;
                                                            subcategory11.title =
                                                              user.name;
                                                            subcategory11.key =
                                                              user.id;
                                                            await Promise.all(
                                                              //nivel12
                                                              subcategorysub11.data.map(
                                                                async (
                                                                  subcategory12
                                                                ) => {
                                                                  console.log(
                                                                    "entra3"
                                                                  );
                                                                  const subcategorysub12 = await app
                                                                    .service(
                                                                      "member-log"
                                                                    )
                                                                    .find({
                                                                      query: {
                                                                        parent_id:
                                                                          subcategory12.id,
                                                                        $sort: {
                                                                          createdAt: -1,
                                                                        },
                                                                      },
                                                                    });
                                                                  const user = await app
                                                                    .service(
                                                                      "users"
                                                                    )
                                                                    .get(
                                                                      subcategory12.id
                                                                    );
                                                                  subcategory12.image =
                                                                    urlimages +
                                                                    user.picture_url;
                                                                  subcategory12.title =
                                                                    user.name;
                                                                  subcategory12.key =
                                                                    user.id;
                                                                  subcategory12.ultimo = 1;
                                                                }
                                                              )
                                                            );
                                                            subcategory11.image =
                                                              urlimages +
                                                              user.picture_url;
                                                            subcategory11.title =
                                                              user.name;
                                                            subcategory11.key =
                                                              user.id;

                                                            subcategory11.children =
                                                              subcategorysub11.data;
                                                          }
                                                        )
                                                      );
                                                      const user = await app
                                                        .service("users")
                                                        .get(subcategory10.id);
                                                      subcategory10.image =
                                                        urlimages +
                                                        user.picture_url;
                                                      subcategory10.title =
                                                        user.name;
                                                      subcategory10.key =
                                                        user.id;
                                                      subcategory10.children =
                                                        subcategorysub10.data;
                                                    }
                                                  )
                                                );
                                                const user = await app
                                                  .service("users")
                                                  .get(subcategory9.id);
                                                subcategory9.image =
                                                  urlimages + user.picture_url;
                                                subcategory9.title = user.name;
                                                subcategory9.key = user.id;
                                                subcategory9.children =
                                                  subcategorysub9.data;
                                              }
                                            )
                                          );
                                          const user = await app
                                            .service("users")
                                            .get(subcategory8.id);
                                          subcategory8.image =
                                            urlimages + user.picture_url;
                                          subcategory8.title = user.name;
                                          subcategory8.key = user.id;
                                          subcategory8.children =
                                            subcategorysub8.data;
                                        }
                                      )
                                    );
                                    const user = await app
                                      .service("users")
                                      .get(subcategory7.id);
                                    subcategory7.image =
                                      urlimages + user.picture_url;
                                    subcategory7.title = user.name;
                                    subcategory7.key = user.id;
                                    subcategory7.children =
                                      subcategorysub7.data;
                                  }
                                )
                              );
                              const user = await app
                                .service("users")
                                .get(subcategory6.id);
                              subcategory6.image = urlimages + user.picture_url;
                              subcategory6.title = user.name;
                              subcategory6.key = user.id;
                              subcategory6.children = subcategorysub6.data;
                            })
                          );
                          const user = await app
                            .service("users")
                            .get(subcategory5.id);
                          subcategory5.image = urlimages + user.picture_url;
                          subcategory5.title = user.name;
                          subcategory5.key = user.id;
                          subcategory5.children = subcategorysub5.data;
                        })
                      );
                      const user = await app
                        .service("users")
                        .get(subcategory4.id);
                      subcategory4.image = urlimages + user.picture_url;
                      subcategory4.title = user.name;
                      subcategory4.key = user.id;
                      subcategory4.children = subcategorysub4.data;
                    })
                  );
                  const user = await app.service("users").get(subcategory3.id);
                  subcategory3.image = urlimages + user.picture_url;
                  subcategory3.title = user.name;
                  subcategory3.key = user.id;
                  subcategory3.children = subcategorysub3.data;
                })
              );
              const user = await app.service("users").get(subcategory2.id);
              subcategory2.image = urlimages + user.picture_url;
              subcategory2.title = user.name;
              subcategory2.key = user.id;
              subcategory2.children = subcategorysub2.data;
            })
          );

          const user = await app.service("users").get(subcategory.id);
          subcategory.image = urlimages + user.picture_url;
          subcategory.title = user.name;
          subcategory.key = user.id;
          subcategory.children = subcategorysub.data;
        })
      );
    }

    context.result = categories;
    return context;
  };
};
