// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars

module.exports = (options = {}) => {
  return async context => {
    const { app, method, result, params } = context;

  var date = new Date().getTime();
      //console.log(params.query.code);

  var user =  await context.app.service('users').find({
      query:{
        reset: params.query.code
      }
    });

   //console.log(context);
   var mili = date-user.data[0].datereset;
   var segundos = mili/1000;
   var minutos = segundos/60;
   //console.log(minutos);
   if(minutos<30){
    var users = await context.app.service('users').patch(user.data[0].id, {reset: 0, datereset: date });
  }else{
    
    context.result = {response:2}
  }

  if(users){
    context.result =  {response:1, id: users.id}

  }else{
    context.result =  {response:2}
  }
      
    return context;
  };
};
