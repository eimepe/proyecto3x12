// eslint-disable-next-line no-unused-vars

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
var fs = require("fs");

module.exports = function (options = {}) {
  // eslint-disable-line no-unused-vars
  return async (context) => {
    const { data } = context;

    var imgfile = context.data.photo;

    if (imgfile) {
      console.log(imgfile);
      // The actual message text
      var images = null;

      var imgbase = imgfile.split(",");

      var imgvalidate = imgfile.split(".");

      if (
        imgvalidate[1] == "jpg" ||
        imgvalidate[1] == "png" ||
        imgvalidate[1] == "jpeg"
      ) {
        var img = imgfile;
      } else {
        var base64Data = imgbase[1];

        var nombreimg = new Date().getTime() + "banners.png";

        // grabas la imagen el disco
        fs.writeFile(
          "./public/images/banners/" + nombreimg,
          base64Data,
          "base64",
          function (err) {
            console.log(err);
          }
        );

        images = nombreimg;
      }

      context.data = {
        title: context.data.title,
        description: context.data.description,
        photo: images,
        button_text: context.data.button_text,
        button_link: context.data.button_link,
        text_align: context.data.text_align,
        status: context.data.status,
        createdAt: new Date().getTime(),
        updatedAt: new Date().getTime(),
      };
    }

    // Best practice: hooks should always return the context
    return context;
  };
};
