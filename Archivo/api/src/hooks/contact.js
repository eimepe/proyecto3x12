// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars

var nodemailer = require("nodemailer");
module.exports = (options = {}) => {
  return async (context) => {
    const { app, method, result, params } = context;

    var transporter = nodemailer.createTransport({
      service: "Gmail",
      auth: {
        user: "noreply@webbost.net",
        pass: "1Q2w3e4r5t",
      },
    });

    var number = Math.floor(Math.random() * 9999) + 1000;
    console.log(context.data);
    var mailOptions = {
      from: "noreply@3x12.net",
      to: "info@3x12.net",
      subject: "Formulario contacto 3x12",
      text: "",
      html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
      
      <head>
          <meta charset="UTF-8">
          <meta content="width=device-width, initial-scale=1" name="viewport">
          <meta name="x-apple-disable-message-reformatting">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta content="telephone=no" name="format-detection">
          <title></title>
          <!--[if (mso 16)]>
          <style type="text/css">
          a {text-decoration: none;}
          </style>
          <![endif]-->
          <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
          <!--[if gte mso 9]>
      <xml>
          <o:OfficeDocumentSettings>
          <o:AllowPNG></o:AllowPNG>
          <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
          <!--[if !mso]><!-- -->
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
          <!--<![endif]-->
      </head>
      
      <body>
          <div class="es-wrapper-color">
              <!--[if gte mso 9]>
            <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
              <v:fill type="tile" color="#f8f9fd"></v:fill>
            </v:background>
          <![endif]-->
              <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
                  <tbody>
                      <tr>
                          <td class="esd-email-paddings" valign="top">
                              <table cellpadding="0" cellspacing="0" class="es-content esd-footer-popover" align="center">
                                  <tbody>
                                      <tr>
                                          <td class="esd-stripe" align="center" bgcolor="#ffffff" style="background-color: #ffffff;">
                                              <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="background-color: transparent;">
                                                  <tbody>
                                                      <tr>
                                                          <td class="esd-structure es-p15t es-p15b es-p20r es-p20l" style="background-color: #da1f19;" bgcolor="#da1f19" align="left" esd-custom-block-id="56108">
                                                              <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]-->
                                                              <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                                  <tbody>
                                                                      <tr>
                                                                          <td class="es-m-p20b esd-container-frame" width="270" align="left">
                                                                              <table width="100%" cellspacing="0" cellpadding="0">
                                                                                  <tbody>
                                                                                      <tr>
                                                                                          <td class="esd-block-image es-m-txt-c" align="left" style="font-size: 0px;"><a target="_blank"><img src="http://3x12.net/static/media/logo.9b501d90.png" alt style="display: block;" width="168"></a></td>
                                                                                      </tr>
                                                                                  </tbody>
                                                                              </table>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                              <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]-->
                                                              <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                                                  <tbody>
                                                                      <tr>
                                                                          <td class="esd-container-frame" width="270" align="left">
                                                                              <table width="100%" cellspacing="0" cellpadding="0">
                                                                                  <tbody>
                                                                                      <tr>
                                                                                          <td class="esd-block-social es-m-txt-c" align="right" style="font-size: 0px;">
                                                                                              <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0">
                                                                                                  <tbody>
                                                                                                      <tr>
                                                                                                          <td class="es-p10r" valign="top" align="center"><a target="_blank" href="https://www.facebook.com/3-x-12-Panam%C3%A1-114379403698391/"><img title="Facebook" src="https://hjfsrt.stripocdn.email/content/assets/img/social-icons/logo-black/facebook-logo-black.png" alt="Fb" width="32"></a></td>
                                                                                                          <td valign="top" align="center"><a target="_blank" href><img title="Instagram" src="https://hjfsrt.stripocdn.email/content/assets/img/social-icons/logo-black/instagram-logo-black.png" alt="Inst" width="32"></a></td>
                                                                                                      </tr>
                                                                                                  </tbody>
                                                                                              </table>
                                                                                          </td>
                                                                                      </tr>
                                                                                  </tbody>
                                                                              </table>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                              <!--[if mso]></td></tr></table><![endif]-->
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td class="esd-structure es-p30t es-p30b es-p30r es-p30l" align="left">
                                                              <table cellpadding="0" cellspacing="0" width="100%">
                                                                  <tbody>
                                                                      <tr>
                                                                          <td width="540" class="esd-container-frame" align="center" valign="top">
                                                                              <table cellpadding="0" cellspacing="0" width="100%">
                                                                                  <tbody>
                                                                                      <tr>
                                                                                          <td align="left" class="esd-block-text">
                                                                                              <p>Solicitud de contacto<span data-cke-bookmark="1" style="display: none;">&nbsp;</span></p>
                                                                                          </td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td align="center" class="esd-block-button es-p10"><span class="es-button-border es-button-border-1596491319505" style="background: #f6ee0a;"><a href class="es-button es-button-1596491319485" target="_blank" style="background: #f6ee0a;, font-size: 18px; border-color: #f6ee0a; color: #333333;">
                                                                                          Nombre: ${context.data.nombre} <br/>
                                                                                          Telefono: ${context.data.telefono} <br/>
                                                                                          Correo: ${context.data.email} <br/>
                                                                                          Asunto: ${context.data.asunto}  <br/>
                                                                                          Mensaje: ${context.data.mensaje}
      
                                                                                          </a></span></td>
                                                                                      </tr>
                                                                                  </tbody>
                                                                              </table>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                          </td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </div>
      </body>
      
      </html>`,
    };

    var date = new Date().getTime();

    console.log(context.data.email);

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        context.data.response = "2";
        //res.send(500, err.message);
      } else {
        console.log("Email sent");
        //res.status(200).jsonp(req.body);
        context.result.response = "1";
      }
    });

    return context;
  };
};
