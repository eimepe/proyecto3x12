// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    var userr = context.result;

    if (userr.Matriculador == 0) {
      userr.matri = {};
    } else if (userr.Matriculador == null) {
      userr.patri = {};
    } else {
      const user = await context.app.service("users").get(userr.Matriculador);

      userr.matri = user;
    }

    if (userr.Patrocinador == 0) {
      userr.patri = {};
    } else if (userr.Patrocinador == null) {
      userr.patri = {};
    } else {
      const user = await context.app.service("users").get(userr.Patrocinador);

      userr.patri = user;
    }

    return context;
  };
};
