const { AuthenticationService, JWTStrategy } = require('@feathersjs/authentication');
const { LocalStrategy } = require('@feathersjs/authentication-local');
const { expressOauth } = require('@feathersjs/authentication-oauth');
const { AuthenticationBaseStrategy } = require('@feathersjs/authentication');

class facestrategy extends AuthenticationBaseStrategy {
  async authenticate(authentication, params) {

  

    return {
      authentication: { strategy: "face" },
      user: authentication.user
    }
  }
}

module.exports = app => {
  const authentication = new AuthenticationService(app);

  authentication.register('jwt', new JWTStrategy());
  authentication.register('face', new facestrategy());
  authentication.register('local', new LocalStrategy());

  app.use('/authentication', authentication);
  app.configure(expressOauth());
};
