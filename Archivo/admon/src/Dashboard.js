import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Title } from 'react-admin';

export default () => (
    <Card>
        <Title title="Bienvenido al administrador de 3x12" />
<CardContent>Desde aqui podrás ver las graficas y tendencias de ventas (insertar graficas aqui)</CardContent>
    </Card>
);