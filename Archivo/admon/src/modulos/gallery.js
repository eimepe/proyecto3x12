import React, { useCallback, useEffect, useState } from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
  SaveButton,
  useCreate,
  useRedirect,
  useNotify,
  Toolbar,
  SelectInput,
} from "react-admin";
import { Field, useForm, useFormState } from "react-final-form";
import RichTextInput from "ra-input-rich-text";
import Constants from "../Constants";

const SaveWithNoteButton = ({ handleSubmitWithRedirect, ...props }) => {
  const [create] = useCreate("contents");
  const redirectTo = useRedirect();
  const notify = useNotify();
  const { basePath, redirect } = props;

  const form = useForm();
  const fordata = useFormState();

  const handleClick = useCallback(() => {
    console.log(form.getState().values);
    if (form.getState().values.name) {
      try {
        const reader = new FileReader();
        console.log(form.getState().values.name);
        reader.readAsDataURL(form.getState().values.name.rawFile);
        reader.onload = () => {
          form.change("name", reader.result);
          form.submit();
        };
      } catch (error) {
        console.log(Error);
        form.submit();
      }
    } else {
      form.submit();
    }

    //handleSubmitWithRedirect("edit");
  }, [form]);

  return <SaveButton {...props} handleSubmitWithRedirect={handleClick} />;
};

const Toolbarcontent = (props) => (
  <Toolbar {...props}>
    <SaveWithNoteButton
      label="Guardar"
      submitOnEnter={true}
      variant="contained"
      color="primary"
    />
  </Toolbar>
);

const PreviewImage = ({ record, source }) => {
  if (typeof record == "string") {
    record = {
      [source]: `${Constants.URL}/images/gallery/${record}`,
    };
  }
  return <ImageField record={record} source={source} />;
};

const Categoria = ({ record, source }) => {
  const [cate, getCate] = useState("");
  if (typeof record) {
    var result = null;

    fetch(`${Constants.URL}/gallery-category/${record.category_id}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        result = console.log(res.name);
        getCate(res.name);
      })
      .catch((error) => {
        console.log(error);
        result = "Error";
      });
    record = {
      [source]: cate,
    };
    console.log(result);
    console.log(record.category_id);
  }

  return <TextField record={record} source={source} />;
};

export const GalleryList = (props) => (
  <List {...props} title="Contenidos">
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <Categoria source="category_id" label="Categoria" />

      <TextField source="description" />
      <TextField source="createdAt" label="Fecha de creacion" />
    </Datagrid>
  </List>
);

function choices() {
  fetch(`${Constants.URL}/gallery-category?$limit=1000`, {
    method: "GET",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((res) => {
      return res.data;
    })
    .catch((error) => console.log(error));
  return [];
}

export const GalleryEdit = (props) => {
  const [data, setData] = useState([]);

  const getData = () => {
    fetch(`${Constants.URL}/gallery-category?$limit=1000`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setData(res.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    // Actualiza el título del documento usando la API del navegador
    getData();
    console.log("useeff");
  }, []);

  return (
    <Edit {...props}>
      <SimpleForm toolbar={<Toolbarcontent />}>
        <div className="oculto">
          <TextInput disabled label="Id" source="id" />
        </div>
        <SelectInput source="category_id" choices={data} />
        <TextInput label="Descripcion" source="description" />

        <ImageInput
          multiple={false}
          source="name"
          label="Imagen"
          accept="image/*"
          placeholder={<p>Seleccionar una imagen</p>}
        >
          <PreviewImage source="src" />
        </ImageInput>
      </SimpleForm>
    </Edit>
  );
};

export const GalleryCreate = (props) => {
  const [data, setData] = useState([]);

  const getData = () => {
    fetch(`${Constants.URL}/gallery-category?$limit=1000`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setData(res.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    // Actualiza el título del documento usando la API del navegador
    getData();
    console.log("useeff");
  }, []);

  return (
    <Create {...props}>
      <SimpleForm toolbar={<Toolbarcontent />}>
        <div className="oculto">
          <TextInput disabled label="Id" source="id" />
        </div>
        <SelectInput source="category_id" choices={data} />

        <TextInput label="Descripcion" source="description" />

        <ImageInput
          multiple={false}
          source="name"
          label="Imagen"
          accept="image/*"
          placeholder={<p>Seleccionar una imagen</p>}
        >
          <PreviewImage source="src" />
        </ImageInput>
      </SimpleForm>
    </Create>
  );
};
