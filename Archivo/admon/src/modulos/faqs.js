import React from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
} from "react-admin";

import RichTextInput from "ra-input-rich-text";

export const FaqList = (props) => (
  <List {...props} title="Preguntas frecuentes">
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField label="Pregunta" source="question" />
      <TextField label="Respuesta" source="answer" />
      <TextField label="Creado" source="create_date" />
    </Datagrid>
  </List>
);

export const FaqEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <div className="oculto">
        <TextInput disabled label="Id" source="id" />
      </div>

      <TextInput label="Pregunta" source="question" />
      <TextInput label="Respuesta" source="answer" />
    </SimpleForm>
  </Edit>
);

export const FaqCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="Pregunta" source="question" />
      <TextInput label="Respuesta" source="answer" />
    </SimpleForm>
  </Create>
);
