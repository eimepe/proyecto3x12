import React from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
} from "react-admin";

import RichTextInput from "ra-input-rich-text";

export const DiccionarioList = (props) => (
  <List {...props} title="Preguntas frecuentes">
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField label="LLave" source="keyd" />
      <TextField label="Valor" source="valued" />
    </Datagrid>
  </List>
);

export const DiccionarioEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <div className="oculto">
        <TextInput disabled label="Id" source="id" />
      </div>

      <TextInput label="Clave" source="keyd" disabled />
      <TextInput label="Valor" source="valued" />
    </SimpleForm>
  </Edit>
);

export const DiccionarioCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="Clave" source="keyd" />
      <TextInput label="Valor" source="valued" />
    </SimpleForm>
  </Create>
);
