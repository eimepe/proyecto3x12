import React, { useCallback } from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
  SaveButton,
  useCreate,
  useRedirect,
  useNotify,
  Toolbar,
} from "react-admin";
import { Field, useForm, useFormState } from "react-final-form";
import RichTextInput from "ra-input-rich-text";
import Constants from "../Constants";
const SaveWithNoteButton = ({ handleSubmitWithRedirect, ...props }) => {
  const [create] = useCreate("contents");
  const redirectTo = useRedirect();
  const notify = useNotify();
  const { basePath, redirect } = props;

  const form = useForm();
  const fordata = useFormState();

  const handleClick = useCallback(() => {
    console.log(form.getState().values);
    if (form.getState().values.photo) {
      try {
        const reader = new FileReader();
        console.log(form.getState().values.photo);
        reader.readAsDataURL(form.getState().values.photo.rawFile);
        reader.onload = () => {
          form.change("photo", reader.result);
          form.submit();
        };
      } catch (error) {
        console.log(Error);
        form.submit();
      }
    } else {
      form.submit();
    }

    //handleSubmitWithRedirect("edit");
  }, [form]);

  return <SaveButton {...props} handleSubmitWithRedirect={handleClick} />;
};

const Toolbarcontent = (props) => (
  <Toolbar {...props}>
    <SaveWithNoteButton
      label="Guardar"
      submitOnEnter={true}
      variant="contained"
      color="primary"
    />
  </Toolbar>
);

const PreviewImage = ({ record, source }) => {
  if (typeof record == "string") {
    record = {
      [source]: `${Constants.URL}/images/banners/${record}`,
    };
  }
  return <ImageField record={record} source={source} />;
};
export const BannersList = (props) => (
  <List {...props} title="Contenidos">
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="title" label="Titulo" />
      <TextField source="description" />
      <BooleanField source="status" />
      <TextField source="createdAt" label="Fecha de creacion" />
    </Datagrid>
  </List>
);

export const BannersEdit = (props) => (
  <Edit {...props}>
    <SimpleForm toolbar={<Toolbarcontent />}>
      <div className="oculto">
        <TextInput disabled label="Id" source="id" />
      </div>
      <div className="estilo">
        <TextInput label="Titulo" source="title" />
      </div>
      <TextInput label="Descripcion" source="description" />

      <ImageInput
        multiple={false}
        source="photo"
        label="photo"
        accept="image/*"
        placeholder={<p>Seleccionar una imagen</p>}
      >
        <PreviewImage source="src" />
      </ImageInput>
      <BooleanInput label="Activo" source="status" />
      <TextInput label="Texto de boton" source="button_text" />
      <TextInput label="Link de boton" source="button_link" />
      <TextInput label="Alineado de boton(right o left)" source="text_align" />
    </SimpleForm>
  </Edit>
);

export const BannersCreate = (props) => (
  <Create {...props}>
    <SimpleForm variant="outlined" toolbar={<Toolbarcontent />}>
      <div className="oculto">
        <TextInput disabled label="Id" source="id" />
      </div>
      <div className="estilo">
        <TextInput label="Titulo" source="title" />
      </div>
      <TextInput label="Menu" source="menu" />
      <TextInput label="Descripcion" source="description" />
      <RichTextInput label="Contenido" source="content" />

      <ImageInput
        multiple={false}
        source="photo"
        label="Photo"
        accept="image/*"
        placeholder={<p>Seleccionar una imagen</p>}
      >
        <ImageField source="photo" />
      </ImageInput>

      <BooleanInput label="Activo" source="status" />
    </SimpleForm>
  </Create>
);
