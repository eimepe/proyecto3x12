import React from "react";
import { Admin, Resource, ListGuesser } from "react-admin";
//import jsonServerProvider from 'ra-data-json-server';
import { restClient } from "ra-data-feathers";
import feathersClient from "./feathersClient";
import Dashboard from "./Dashboard";
import aut from "./modulos/configauth/aut";

import { ContentList, ContentEdit, ContentCreate } from "./modulos/contents";
import { FaqList, FaqEdit, FaqCreate } from "./modulos/faqs";

import { GalleryList, GalleryEdit, GalleryCreate } from "./modulos/gallery";
import {
  DiccionarioList,
  DiccionarioEdit,
  DiccionarioCreate,
} from "./modulos/diccionario";
import { BannersList, BannersEdit, BannersCreate } from "./modulos/banners";
import { NewsList, NewsEdit, NewsCreate } from "./modulos/news";
import {
  CategorygList,
  CategorygEdit,
  CategorygCreate,
} from "./modulos/category-gallery";
import { UsersaList, UsersaEdit, UsersaCreate } from "./modulos/usersactivate";

const restClientOptions = {
  id: "id", // In this example, the database uses '_id' rather than 'id'
  usePatch: true, // Use PATCH instead of PUT for updates
};
const options = {
  storageKey: "accessToken", // The key in localStorage used to store the authentication token
  authenticate: {
    // Options included in calls to Feathers client.authenticate
    strategy: "local", // The authentication strategy Feathers should use
  },
  permissionsKey: "permissions", // The key in localStorage used to store permissions from decoded JWT
  permissionsField: "role", // The key in the decoded JWT containing the user's role
  passwordField: "password", // The key used to provide the password to Feathers client.authenticate
  usernameField: "email", // The key used to provide the username to Feathers client.authenticate
  redirectTo: "/login", // Redirect to this path if an AUTH_CHECK fails. Uses the react-admin default of '/login' if omitted.
  logoutOnForbidden: true, // Logout when response status code is 403
};
const dataProviderApp = restClient(feathersClient, restClientOptions);
const authProviders = aut(feathersClient, options);

//const dataProviderExample = jsonServerProvider('https://jsonplaceholder.typicode.com');

const App = () => (
  <Admin
    title="Administrador 3x12"
    locale="es"
    dashboard={Dashboard}
    dataProvider={dataProviderApp}
    authProvider={authProviders}
  >
    {(permissions) => [
      permissions > 0 ? (
        <Resource
          title="Contenidos"
          name="contents"
          options={{ label: "Contenidos" }}
          list={ContentList}
          edit={ContentEdit}
          create={ContentCreate}
        />
      ) : null,

      permissions === "1" ? (
        <Resource
          title="Banners"
          name="banners"
          options={{ label: "Imagenes del banner" }}
          list={BannersList}
          edit={BannersEdit}
          create={BannersCreate}
        />
      ) : null,

      permissions === "1" ? (
        <Resource
          title="Noticias"
          name="news"
          options={{ label: "Noticias" }}
          list={NewsList}
          edit={NewsEdit}
          create={NewsCreate}
        />
      ) : null,

      permissions === "1" ? (
        <Resource
          title="Preguntas frecuentes"
          name="faqs"
          options={{ label: "Preguntas frecuentes" }}
          list={FaqList}
          edit={FaqEdit}
          create={FaqCreate}
        />
      ) : null,
      permissions === "1" ? (
        <Resource
          title="Activar usuarios"
          name="users"
          options={{ label: "Activar usuarios" }}
          list={UsersaList}
          edit={UsersaEdit}
        />
      ) : null,

      permissions === "1" ? (
        <Resource
          title="Categorias de galeria"
          name="gallery-category"
          options={{ label: "Categorias Galeria" }}
          list={CategorygList}
          edit={CategorygEdit}
          create={CategorygCreate}
        />
      ) : null,
      permissions === "1" ? (
        <Resource
          title="Galeria"
          name="gallery"
          options={{ label: "Galeria" }}
          list={GalleryList}
          edit={GalleryEdit}
          create={GalleryCreate}
        />
      ) : null,

      permissions === "1" ? (
        <Resource
          title="Diccionario"
          name="diccionario"
          options={{ label: "Textos de la web" }}
          list={DiccionarioList}
          edit={DiccionarioEdit}
          create={DiccionarioCreate}
        />
      ) : null,
    ]}
  </Admin>
);

export default App;
