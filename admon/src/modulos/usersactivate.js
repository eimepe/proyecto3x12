import React, { useCallback } from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
  useCreate,
  useRedirect,
  useNotify,
  Filter,
  SaveButton,
  Toolbar,
} from "react-admin";
import { Field, useForm, useFormState } from "react-final-form";
import RichTextInput from "ra-input-rich-text";
const Statuspago = ({ record, source }) => {
  if (typeof record) {
    record = {
      [source]: record.ifpago == 1 ? true : false,
    };
  }
  return <BooleanField record={record} source={source} />;
};

const PostFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Buscar por Codigo" source="id" alwaysOn />
  </Filter>
);
export const UsersaList = (props) => (
  <List {...props} filters={<PostFilter />} title="Preguntas frecuentes">
    <Datagrid rowClick="edit">
      <TextField source="id" label="Codigo " />
      <TextField label="Nombre" source="name" />
      <Statuspago
        label="Pago Inicial"
        source="ifpago"
        valueLabelTrue={1}
        valueLabelFalse={0}
      />
      <TextField label="Creado" source="create_date" />
    </Datagrid>
  </List>
);

const SaveWithNoteButton = ({ handleSubmitWithRedirect, ...props }) => {
  const [create] = useCreate("contents");
  const redirectTo = useRedirect();
  const notify = useNotify();
  const { basePath, redirect } = props;

  const form = useForm();
  const fordata = useFormState();

  const handleClick = useCallback(() => {
    console.log(form.getState().values);

    const confirmar = window.confirm(
      "Estas seguro que quieres Cambiar el estado de pago?"
    );

    if (confirmar) {
      form.submit();
    }

    //handleSubmitWithRedirect("edit");
  }, [form]);

  return <SaveButton {...props} handleSubmitWithRedirect={handleClick} />;
};

const Toolbarcontent = (props) => (
  <Toolbar {...props}>
    <SaveWithNoteButton
      label="Guardar"
      submitOnEnter={true}
      variant="contained"
      color="primary"
    />
  </Toolbar>
);

export const UsersaEdit = (props) => (
  <Edit {...props}>
    <SimpleForm toolbar={<Toolbarcontent />}>
      <TextInput disabled label="Codigo" source="id" />

      <TextInput label="Pregunta" source="name" />
      <BooleanInput label="Activar Pago" source="ifpago" />
    </SimpleForm>
  </Edit>
);

export const UsersaCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="Pregunta" source="question" />
      <TextInput label="Respuesta" source="answer" />
    </SimpleForm>
  </Create>
);
