import React from "react";
import {
  List,
  Edit,
  Create,
  Datagrid,
  SimpleForm,
  ImageInput,
  ImageField,
  TextField,
  TextInput,
  BooleanField,
  BooleanInput,
  EmailField,
  EditButton,
} from "react-admin";

import RichTextInput from "ra-input-rich-text";

export const CategorygList = (props) => (
  <List {...props} title="Preguntas frecuentes">
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField label="Pregunta" source="name" />
      <TextField label="Respuesta" source="description" />
      <TextField label="Creado" source="createdAt" />
    </Datagrid>
  </List>
);

export const CategorygEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <div className="oculto">
        <TextInput disabled label="Id" source="id" />
      </div>

      <TextInput label="Pregunta" source="name" />
      <TextInput label="Respuesta" source="description" />
    </SimpleForm>
  </Edit>
);

export const CategorygCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="Pregunta" source="name" />
      <TextInput label="Respuesta" source="description" />
    </SimpleForm>
  </Create>
);
