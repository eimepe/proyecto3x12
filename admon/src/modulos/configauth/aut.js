import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_CHECK,
  AUTH_ERROR,
  AUTH_GET_PERMISSIONS,
} from "react-admin";
import decodeJwt from "jwt-decode";

export default (client, options = {}) => (type, params) => {
  const {
    storageKey,
    authenticate,
    permissionsKey,
    permissionsField,
    passwordField,
    usernameField,
    redirectTo,
    logoutOnForbidden,
  } = Object.assign(
    {},
    {
      storageKey: "accessToken",
      authenticate: { strategy: "local" },
      permissionsKey: "permissions",
      permissionsField: "role",
      passwordField: "password",
      usernameField: "email",
      logoutOnForbidden: true,
    },
    options
  );

  switch (type) {
    case AUTH_LOGIN:
      const { username, password } = params;
      console.log(params);
      const login = client
        .authenticate({
          ...authenticate,
          [usernameField]: username,
          [passwordField]: password,
        })
        .then((params) => {
          try {
            if (params.user.role == 0) {
              localStorage.removeItem(storageKey);
              localStorage.removeItem(permissionsKey);
              return Promise.reject();
            }
            const jwtPermissions = params.user.role;
            console.log(jwtPermissions);
            localStorage.setItem(
              permissionsKey,
              JSON.stringify(jwtPermissions)
            );
            return Promise.resolve(jwtPermissions);
          } catch (e) {
            return Promise.reject();
          }
        });
      console.log(login);
      return login;
    case AUTH_LOGOUT:
      return client
        .logout()
        .then(() => localStorage.removeItem(permissionsKey));
    case AUTH_CHECK:
      const hasJwtInStorage = !!localStorage.getItem(storageKey);
      const hasReAuthenticate =
        Object.getOwnPropertyNames(client).includes("reAuthenticate") &&
        typeof client.reAuthenticate === "function";

      if (hasJwtInStorage && hasReAuthenticate) {
        return client
          .reAuthenticate()
          .then(() => Promise.resolve())
          .catch(() => Promise.reject({ redirectTo }));
      }
      console.log(client.params);
      return hasJwtInStorage
        ? Promise.resolve()
        : Promise.reject({ redirectTo });
    case AUTH_ERROR:
      const { code } = params;
      if (code === 401 || (logoutOnForbidden && code === 403)) {
        localStorage.removeItem(storageKey);
        localStorage.removeItem(permissionsKey);
        return Promise.reject();
      }
      return Promise.resolve();
    case AUTH_GET_PERMISSIONS:
      console.log(permissionsKey);
      /*
        JWT token may be provided by oauth,
        so that's why the permissions are decoded here and not in AUTH_LOGIN.
        */
      // Get the permissions from localstorage if any.
      const localStoragePermissions = JSON.parse(
        localStorage.getItem(permissionsKey)
      );
      // If any, provide them.
      if (localStoragePermissions) {
        return Promise.resolve(localStoragePermissions);
      }
    // Or find them from the token, save them and provide them.

    default:
      return Promise.reject(
        `Unsupported FeathersJS authClient action type ${type}`
      );
  }
};
