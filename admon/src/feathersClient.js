import Constants from "./Constants";

const feathers = require("@feathersjs/feathers");
const rest = require("@feathersjs/rest-client");
const auth = require("@feathersjs/authentication-client");
// Connect to a different URL

const host = Constants.URL;

export default feathers()
  .configure(rest(host).fetch(window.fetch.bind(window)))
  .configure(
    auth({
      jwtStrategy: "jwt",
      storageKey: "accessToken",
      storage: window.localStorage,
    })
  );
