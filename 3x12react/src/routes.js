import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Landing from "./containers/Landing";
import Nosotros from "./containers/Nosotros";
import Faqs from "./containers/Faqs";
import Galeria from "./containers/Galeria";
import Noticias from "./containers/Noticias";
import Noticia from "./containers/Noticia";
import Contactenos from "./containers/Contactenos";
import Error404 from "./containers/Error404";
import Registro from "./containers/Registro";
import Contenido from "./containers/Contenido";
import Micuenta from "./containers/Micuenta";
import Login from "./containers/Login";
import Lineal from "./containers/cuenta/Lineal";
import Piramidal from "./containers/cuenta/Piramidal";
import Pago from "./containers/cuenta/Pago";
import Resetpass from "./containers/Resetpass";
import piramideapp from "./containers/piramideapp";

const AppRoutes = () => (
  <Switch>
    <Route path="/login" component={Login} />
    <Route path="/pago" component={Pago} />
    <Route path="/resetpass" component={Resetpass} />
    <Route path="/piramideapp" component={piramideapp} />
    <Route path="/estructuralineal" component={Lineal} />
    <Route path="/piramidal" component={Piramidal} />
    <Route path="/micuenta" component={Micuenta} />
    <Route path="/nosotros" component={Nosotros} />
    <Route path="/galeria" component={Galeria} />
    <Route path="/register" component={Registro} />
    <Route path="/faqs" component={Faqs} />
    <Route path="/noticias" component={Noticias} />
    <Route path="/contactenos" component={Contactenos} />
    <Route path="/landing" component={Landing} />
    <Route path="/" component={Home} exact={true} />
    <Route path="/contenido/:url" component={Contenido} />
    <Route path="/noticia/:url" component={Noticia} />

    <Route component={Error404} />
  </Switch>
);

export default AppRoutes;
