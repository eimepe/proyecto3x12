import React from 'react';

import { Row,Button,Col } from "antd";
import { Link } from "react-router-dom";

class ButtonAfiliar extends React.Component {
    render() {
      return (
        <Row justify="center" className="text-center espacioafiliate" >
            <Col span={12}>
              <Button type="primary" className="btn-registro-lg">
                  <Link to="/register"> Afíliate AHORA!</Link>
              </Button>
            </Col>
        </Row>
      ); 
    }
  }
  
  export default ButtonAfiliar;
  