import React from "react";
import { connect } from "react-redux";

import { setDiccionario } from "../../redux/actions";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import "../../static/css/custom.css";

import { Layout, Menu, Breadcrumb, Button } from "antd";
import Constants from "../../utils/Constants";
import FooterPagina from "../Web/FooterPagina";
import MenuPagina from "../Web/MenuPagina";
import MenuMobileWeb from "../Web/MenuMobileWeb";
const { Header, Content, Footer, Sider } = Layout;

class LayoutPagina extends React.Component {
  constructor(props) {
    super(props);
  }

  getDiccionario() {
    fetch(`${Constants.URL}/diccionario?$limit=50`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        var diccionario = {};

        res.data.map((dicc) => {
          diccionario[dicc.keyd] = dicc.valued;
        });

        this.props.setDiccionario(diccionario);
        console.log(res.data)
        console.log(diccionario)
      });
  }

  componentDidMount() {
    this.getDiccionario();
  }
  render() {
    return (
      <Layout>
        <Header>
          <div className="logo">
            <img src={logo} />
          </div>
          <MenuPagina />
          <MenuMobileWeb />
        </Header>
        <Content>{this.props.children}</Content>

        <FooterPagina />
      </Layout>
    );
  }
}

const mapStateProps = (state) => {
  return {
    diccionario: state.diccionario,
  };
};

const mapDispatchToProps = {
  setDiccionario: setDiccionario,
};

export default connect(mapStateProps, mapDispatchToProps)(LayoutPagina);
