import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import { Layout, Menu, Breadcrumb, Button } from "antd";
import Menuprincipal from "../Menuprincipal";

import FooterPagina from "../Web/FooterPagina";
import MenuMobileWeb from "../Web/MenuMobileWeb";
const { Header, Content, Footer, Sider } = Layout;

class LayoutInicial extends React.Component {
  render() {
    return (
      <Layout>
        {/* <Sider
        breakpoint="sm"
        collapsedWidth="1"
        onCollapse={(collapsed, type) => { console.log(collapsed, type); }}
        style={{ minHeight: "100vh" }}
        /> */}
        <Header>
          <div className="logo">
            <img src={logo} />
          </div>
          <Menuprincipal />
          <MenuMobileWeb />
        </Header>
        <Content>{this.props.children}</Content>

        <FooterPagina />
      </Layout>
    );
  }
}

export default LayoutInicial;
