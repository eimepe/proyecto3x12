import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import "../../static/css/custom.css";
import { Layout, Menu, Breadcrumb, Button } from "antd";

import FooterPagina from "../Web/FooterPagina";
import MenuPagina from "../Web/MenuPagina";
import MenuMobileWeb from "../Web/MenuMobileWeb";
import Banner from "../Web/Banner";
import { setDiccionario } from "../../redux/actions";
import { connect } from "react-redux";
import Constants from "../../utils/Constants";

const { Header, Content, Footer, Sider } = Layout;

class LayoutHome extends React.Component {
  constructor(props) {
    super(props);
  }

  getDiccionario() {
    fetch(`${Constants.URL}/diccionario`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        var diccionario = {};

        res.data.map((dicc) => {
          diccionario[dicc.keyd] = dicc.valued;
        });

        this.props.setDiccionario(diccionario);
      });
  }

  componentDidMount() {
    this.getDiccionario();
  }
  render() {
    return (
      <Layout>
        <Header>
          <div className="logo">
            <img src={logo} />
          </div>
          <MenuPagina />
          <MenuMobileWeb />
        </Header>
        <Banner />
        <Content>{this.props.children}</Content>

        <FooterPagina />
      </Layout>
    );
  }
}

const mapStateProps = (state) => {
  return {
    diccionario: state.diccionario,
  };
};

const mapDispatchToProps = {
  setDiccionario: setDiccionario,
};

export default connect(mapStateProps, mapDispatchToProps)(LayoutHome);
