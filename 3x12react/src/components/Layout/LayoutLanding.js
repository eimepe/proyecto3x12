import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import "../../static/css/custom.css";

import { Layout, Menu, Breadcrumb, Button } from "antd";
import MenuLanding from "../Landing/MenuLanding";

import FooterLanding from "../Landing/FooterLanding";
import MenuMobileLanding from "../Landing/MenuMobileLanding";
const { Header, Content, Footer, Sider } = Layout;

class LayoutLanding extends React.Component {
  render() {
    return (
      <Layout>
        {/* <Sider
        breakpoint="sm"
        collapsedWidth="1"
        onCollapse={(collapsed, type) => { console.log(collapsed, type); }}
        style={{ minHeight: "100vh" }}
        /> */}
        <Header>
          <div className="logo">
            <img src={logo} />
          </div>
          <MenuLanding />
          <MenuMobileLanding />
        </Header>
        <Content>{this.props.children}</Content>

        <FooterLanding />
      </Layout>
    );
  }
}

export default LayoutLanding;
