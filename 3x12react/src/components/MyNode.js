import React from "react";
import PropTypes from "prop-types";
import "./my-node.css";
import { render } from "@testing-library/react";
import Constants from "../utils/Constants";

const propTypes = {
  nodeData: PropTypes.object.isRequired,
};

class MyNode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clase: "ocultar",
    };
  }

  selectNode = () => {
    this.setState({ clase: "mostrar" });
  };

  cerrar() {
    this.setState({ clase: "ocultar" });
  }
  render() {
    console.log("Nombre");
    console.log(this.props.nodeData.name);
    return (
      <>
        <div onClick={this.selectNode}>
          <div
            className={
              this.props.nodeData.status == 1 ? "position" : "position2"
            }
          >
            {this.props.nodeData.title}
          </div>
          <div className="fullname">
            {this.props.nodeData.name
              ? this.props.nodeData.name.toUpperCase()
              : ""}
          </div>
        </div>
        <div id="myModal" className={`modal ${this.state.clase}`}>
          <div className={`modal-content ${this.state.clase}`}>
            <span onClick={this.cerrar.bind(this)} className="close">
              &times;
            </span>
            <img
              style={{ width: "100px", height: "100px", borderRadius: "50%" }}
              src={
                Constants.URL + "/images/profile/" + this.props.nodeData.image
              }
            />
            <p>{this.props.nodeData.title}</p>
            <p>{"Nombre: " + this.props.nodeData.name}</p>
          </div>
        </div>
      </>
    );
  }
}

MyNode.propTypes = propTypes;

export default MyNode;
