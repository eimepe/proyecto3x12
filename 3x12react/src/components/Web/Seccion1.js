import React from "react";
import "../../static/css/style.css";
import { Layout, Row, Col } from "antd";
import Constants from "../../utils/Constants";
import { connect } from "react-redux";

const { Header, Content, Footer, Sider } = Layout;

class Seccion1 extends React.Component {
  render() {
    
    return (
      <Row justify="center" className="text-center espaciohomebanner">
        <Col xs={24} sm={24} md={7} lg={7} xl={7}>
          <div class="single-service service__style--4 large-size text-center">
            <a href="#">
              <div class="service">
                <div class="icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="feather feather-cast"
                  >
                    <path d="M2 16.1A5 5 0 0 1 5.9 20M2 12.05A9 9 0 0 1 9.95 20M2 8V6a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2h-6"></path>
                    <line x1="2" y1="20" x2="2.01" y2="20"></line>
                  </svg>
                </div>
                <div class="content">
                  <h3 class="title">{this.props.diccionario.titulo_1_seccion_1}</h3>
                  <p>
                  {this.props.diccionario.texto_1_seccion_1}
                  </p>
                </div>
              </div>
            </a>
          </div>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} xl={7}>
          <div class="single-service service__style--4 large-size text-center">
            <a href="#">
              <div class="service">
                <div class="icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="feather feather-layers"
                  >
                    <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                    <polyline points="2 17 12 22 22 17"></polyline>
                    <polyline points="2 12 12 17 22 12"></polyline>
                  </svg>
                </div>
                <div class="content">
                  <h3 class="title">{this.props.diccionario.titulo_2_seccion_1}</h3>
                  <p>
                  {this.props.diccionario.texto_2_seccion_1}
                  </p>
                </div>
              </div>
            </a>
          </div>
        </Col>
        <Col xs={24} sm={24} md={7} lg={7} xl={7}>
          <div class="single-service service__style--4 large-size text-center">
            <a href="#">
              <div class="service">
                <div class="icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="feather feather-users"
                  >
                    <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                    <circle cx="9" cy="7" r="4"></circle>
                    <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                  </svg>
                </div>
                <div class="content">
                  <h3 class="title">{this.props.diccionario.titulo_3_seccion_1}</h3>
                  <p>
                  {this.props.diccionario.texto_3_seccion_1}
                  </p>
                </div>
              </div>
            </a>
          </div>
        </Col>
      </Row>
    );
  }
}
const mapStateProps = (state) => {
  return {
    diccionario: state.diccionario,
  };
};

const mapDispatchToProps = {
  //setDiccionario: setDiccionario,
};
export default connect(mapStateProps, mapDispatchToProps)(Seccion1);
