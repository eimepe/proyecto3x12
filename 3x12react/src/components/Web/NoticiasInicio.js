import React from "react";
import "../../static/css/style.css";
import { Layout, Row, Col } from "antd";
import Constants from "../../utils/Constants";
import { connect } from "react-redux";

const { Header, Content, Footer, Sider } = Layout;

class NoticiasInicio extends React.Component {
  state = {
    DataNews: [],
  };

  GetNews = (values) => {
    console.log(values);

    fetch(`${Constants.URL}/news?$limit=3`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ DataNews: res.data }));
  };

  componentDidMount() {
    this.GetNews();
  }

  render() {
    const news = this.state.DataNews;
    console.log(this.props.diccionario);
    return (
      <div class="rn-blog-area rn-section-gap bg_color--1">
        <Row justify="center" className="text-center">
          <Col span={24}>
            <div class="section-title section-title--2 text-center mb--20">
              <h2 class="title">{this.props.diccionario.noticias_novedades}</h2>
              <p>{this.props.diccionario.detalle_noticias_novedades}</p>
            </div>
          </Col>
        </Row>
        <Row justify="center" className="text-center">
          {news.map((noticia) => (
            <Col
              style={{ marginLeft: 20 }}
              xs={12}
              sm={12}
              md={7}
              lg={7}
              xl={7}
            >
              <div class="blog blog-style--1">
                <div class="thumbnail">
                  <a href={"noticia/" + noticia.id}>
                    <img
                      class="w-100"
                      src={`${Constants.URL}/images/news/${noticia.photo}`}
                      alt="Blog Images"
                    />
                  </a>
                </div>
                <div class="content">
                  <p class="blogtype">{noticia.news}</p>
                  <h4 class="title">
                    <a href={"noticia/" + noticia.id}>{noticia.news}</a>
                  </h4>
                  <div class="blog-btn">
                    <a class="rn-btn text-white" href={"noticia/" + noticia.id}>
                      Leer más
                    </a>
                  </div>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

const mapStateProps = (state) => {
  return {
    diccionario: state.diccionario,
  };
};

const mapDispatchToProps = {
  //setDiccionario: setDiccionario,
};
export default connect(mapStateProps, mapDispatchToProps)(NoticiasInicio);
