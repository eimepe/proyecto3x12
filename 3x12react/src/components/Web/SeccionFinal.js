import React from "react";
import "../../static/css/style.css";
import { Layout,Row,Col } from "antd";
import { connect } from "react-redux";

const { Header, Content, Footer, Sider } = Layout;


class SeccionFinal extends React.Component {



    render() {
      
        return (
            <div class="call-to-action-wrapper call-to-action text-white-wrapper  ptb--120">    
                <Row justify="center" className="text-center" >
                    <Col span={24} >
                        <div class="inner text-center"><span>{this.props.diccionario.seccion_final_peq}</span>
                            <h2>{this.props.diccionario.seccion_final_grande}</h2>
                            <a class="rn-button-style--2" href="register">
                                <span>{this.props.diccionario.texto_boton_seccion_final}</span>
                            </a>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateProps = (state) => {
    return {
      diccionario: state.diccionario,
    };
  };
  
  const mapDispatchToProps = {
    //setDiccionario: setDiccionario,
  };
  export default connect(mapStateProps, mapDispatchToProps)(SeccionFinal);
  