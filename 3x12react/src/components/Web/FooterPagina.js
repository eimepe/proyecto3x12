import React from "react";
import { Layout, Menu, Breadcrumb, Button,Row,Col } from "antd";
import { Link } from "react-router-dom";
import logo from "../../static/images/logo.png";
import { connect } from "react-redux";

const { Footer } = Layout;


class FooterIncial extends React.Component {
  render() {
    return <Footer>              
        <div className="footer-style-2 ptb--30 bg_image bg_image--1" data-black-overlay="6">
            <div className="wrapper plr--50 plr_sm--20">
              <Row className="align-items-center justify-content-between">
                    <Col xs={12} sm={8} md={8} lg={8} xl={8}>
                    
                        <div className="inner">
                            <div className="logo text-center text-sm-left mb_sm--20">
                                <a href="#">
                                    <img src={logo} />
                                </a>
                            </div>
                        </div>
                  
                    </Col>
                    <Col xs={12} sm={8} md={8} lg={8} xl={8}>
                        <div className="inner text-center">
                            <ul className="social-share rn-lg-size d-flex justify-content-center liststyle">
                                <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i className="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </Col>
                    <Col xs={24} sm={8} md={8} lg={8} xl={8}>
                        <div className="inner text-lg-right text-center mt_md--20 mt_sm--20">
                            <div className="text">
                                <p>{this.props.diccionario.footer}</p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
          </div>
        
    </Footer>;
  }
}

const mapStateProps = (state) => {
    return {
      diccionario: state.diccionario,
    };
  };
  
  const mapDispatchToProps = {
    //setDiccionario: setDiccionario,
  };
  export default connect(mapStateProps, mapDispatchToProps)(FooterIncial);
  

