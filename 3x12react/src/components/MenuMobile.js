import React, { useState } from "react";
import { Drawer, Button, Menu } from "antd";
import "../static/css/menumobile.css";
import { MenuOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class MenuMobile extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: false,
    };
  }
  scroll = (pos) => {
    const elemento = document.querySelector(pos);

    elemento.scrollIntoView("smooth", "end");
    this.setState({ visible: false });
  };
  showDrawer = () => {
    this.setState({ visible: true });
  };

  onClose = () => {
    this.setState({ visible: false });
  };

  render() {
    return (
      <div className="menumobile">
        <Button type="primary" onClick={this.showDrawer}>
          <MenuOutlined />
        </Button>
        <Drawer
          title="Menu"
          placement="right"
          closable={true}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <Menu theme="light" mode="vertical">
            {/* <MenuMobile/> */}

            <Menu.Item key="1">
              <Link to="/" onClick={() => this.scroll("#inicio")}>
                Inicio
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/" onClick={() => this.scroll("#nosotros")}>
                Nosotros
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/" onClick={() => this.scroll("#oportunidades")}>
                Oportunidades
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/" onClick={() => this.scroll("#contactenos")}>
                Contactenos
              </Link>
            </Menu.Item>

            {this.props.user.accessToken ? (
              <Menu.Item key="5">
                <Link to="/micuenta">Hola( {this.props.user.user.name})</Link>
              </Menu.Item>
            ) : (
              <Menu.Item key="6">
                <Link to="/login">Inicia Sesión</Link>
              </Menu.Item>
            )}

            {this.props.user.accessToken ? null : (
              <Button type="primary" className="btn-afiliate">
                <Link to="/register"> Afíliate AHORA!</Link>
              </Button>
            )}
          </Menu>
        </Drawer>
      </div>
    );
  }
}

const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(MenuMobile);
