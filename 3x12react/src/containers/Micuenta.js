import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/micuenta.css";
import {
  Layout,
  Space,
  Menu,
  Modal,
  Button,
  Row,
  Col,
  Avatar,
  Typography,
} from "antd";
import equal from "esequal";
import OrganizationChart from "@dabeng/react-orgchart";
import MyNode from "../components/MyNode";
import {
  UserOutlined,
  LaptopOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import MetaTags from "react-meta-tags";
import { connect } from "react-redux";
import Layoutcuenta from "../components/Layout/Layoutcuenta";
import { Link, Redirect } from "react-router-dom";
import EditDatoscontacto from "./cuenta/editperfil/EditDatoscontacto";
import EditDatosresidencia from "./cuenta/editperfil/EditDatosresidencia";
import EditDatosmatriculador from "./cuenta/editperfil/EditDatosmatriculador";
import EditDatosbanco from "./cuenta/editperfil/EditDatosbanco";
import Constants from "../utils/Constants";
import { setUser } from "../redux/actions";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const { Title, Text } = Typography;
class Micuenta extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clase: "ocultar",
      ds: {},
      dss: {},
      modalcontacto: false,
      modalresidencia: false,
      modalmatriculador: false,
      modalbancarios: false,
      responsevacios: "Tu perfil esta completo",
    };
    this.getdata = this.getdata.bind(this);
  }
  validarUser() {
    fetch(`${Constants.URL}/users/${this.props.user.user.id}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.code == 401) {
          this.props.setUser({});
        } else {
          if (res.id) {
            var users = this.props.user;
            users.user = res;
            this.props.setUser(users);
          } else {
            console.log("error al guardar");
          }
        }
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  validarCamposvacios() {
    if (
      this.props.user.user.email == null ||
      this.props.user.user.numcelularllamadas == null ||
      this.props.user.user.numcelularwhatsapp == null ||
      this.props.user.user.nacionalidad == null ||
      this.props.user.user.ciudad == null ||
      this.props.user.user.corregimiento == null ||
      this.props.user.user.barriada == null ||
      this.props.user.user.calle == null ||
      this.props.user.user.casa == null ||
      this.props.user.user.ph == null ||
      this.props.user.user.apartamento == null ||
      this.props.user.user.aclaracion == null ||
      this.props.user.user.Matriculador == null ||
      this.props.user.user.Patrocinador == null ||
      this.props.user.user.tipo_identificacion == null ||
      this.props.user.user.numidentificacion == null ||
      this.props.user.user.banco == null ||
      this.props.user.user.numerocuenta == null
    ) {
      this.setState({
        responsevacios:
          "Tu perfil esta incompleto puedes cambiar tus datos a continuacion.",
      });
    } else if (
      this.props.user.user.email == "" ||
      this.props.user.user.numcelularllamadas == "" ||
      this.props.user.user.numcelularwhatsapp == "" ||
      this.props.user.user.nacionalidad == "" ||
      this.props.user.user.ciudad == "" ||
      this.props.user.user.corregimiento == "" ||
      this.props.user.user.barriada == "" ||
      this.props.user.user.calle == "" ||
      this.props.user.user.casa == "" ||
      this.props.user.user.ph == "" ||
      this.props.user.user.apartamento == "" ||
      this.props.user.user.aclaracion == "" ||
      this.props.user.user.Matriculador == "" ||
      this.props.user.user.Patrocinador == "" ||
      this.props.user.user.tipo_identificacion == "" ||
      this.props.user.user.numidentificacion == "" ||
      this.props.user.user.banco == "" ||
      this.props.user.user.numerocuenta == ""
    ) {
      this.setState({
        responsevacios:
          "Tu perfil esta incompleto puedes cambiar tus datos a continuacion.",
      });
    } else {
      this.setState({ responsevacios: "" });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    //this.validarCamposvacios();
  }
  abrir() {}

  getdata() {
    fetch(`${Constants.URL}/member-log?parent_id=${this.props.user.user.id}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        //console.log(res);
        const array = {
          id: this.props.user.user.id,
          title: "Codigo:" + this.props.user.user.id,
          name: this.props.user.user.name,
          image:
            `${Constants.URLFILES}/mlm/uploads/profiles/` +
            this.props.user.user.picture_url,
          children: res.data,
        };

        this.setState({ ds: array });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  cancelModal() {
    this.setState({
      modalcontacto: false,
      modalresidencia: false,
      modalmatriculador: false,
      modalbancarios: false,
    });
    this.validarCamposvacios();
  }

  componentDidMount() {
    this.validarUser();
    this.validarCamposvacios();
    this.getdata();

    if (
      this.props.user.user.Matriculador == null ||
      this.props.user.user.Matriculador == 0 ||
      this.props.user.user.Matriculador == ""
    ) {
      this.setState({ modalmatriculador: true });
    }
  }
  render() {
    if (!this.props.user.accessToken) {
      return <Redirect to="/" />;
    } else {
      return (
        <Layoutcuenta context={this.props.history}>
          <MetaTags>
            <title>Mi cuenta</title>
            <meta name="description" content="Mi plataforma 3x12." />
            <meta property="og:title" content="plataforma 3 x12" />
          </MetaTags>
          <Row justify="center" align="middle" className="header-account">
            <Col span={12}>
              <Avatar
                className={
                  this.props.user.user.status == 0 ||
                  this.props.user.user.status == 1 ||
                  this.props.user.user.status == 2
                    ? "logonopago"
                    : this.props.user.user.status == 3
                    ? "logosi"
                    : this.props.user.user.status == 4
                    ? "logovencido"
                    : null
                }
                size={150}
                icon={
                  this.props.user.user.picture_url ? (
                    <img
                      src={`${Constants.URL}/images/profile/${this.props.user.user.picture_url}`}
                    />
                  ) : (
                    <UserOutlined />
                  )
                }
              />
            </Col>
            <Col span={12}>
              <Title className="titlesaccount" level={3}>
                Hola: {this.props.user.user.name}
              </Title>
              <Text className="textcode" level={4}>
                Codigo de matriculador: {this.props.user.user.id}
              </Text>
              <br />
              {this.props.user.user.ifpago !== 1 ? (
                <>
                  <Text className="textpago" level={4}>
                    Aun no has realizado tu pago inicial
                  </Text>
                  <br />
                  <Button type="primary">
                    <Link to="/pago">Realizar Pago</Link>
                  </Button>
                </>
              ) : null}
            </Col>
          </Row>
          <Title level={4} style={{ color: "red", margin: 20 }}>
            {this.state.responsevacios}
          </Title>
          <Row className="tarjetas">
            <Col span={24}>
              <Space direction="vertical">
                <Title level={4}>Datos de contacto</Title>
                <Text>Correo: {this.props.user.user.email}</Text>
                <Text>
                  Celular llamadas: {this.props.user.user.numcelularllamadas}
                </Text>
                <Text>
                  Celular Whatsapp: {this.props.user.user.numcelularwhatsapp}
                </Text>
                <Button
                  type="primary"
                  onClick={() => this.setState({ modalcontacto: true })}
                >
                  Editar
                </Button>
              </Space>
            </Col>
          </Row>

          <Row className="tarjetas">
            <Col span={24}>
              <Space direction="vertical">
                <Title level={4}>Datos de Residencia</Title>
                <Text>Nacionalidad: {this.props.user.user.nacionalidad}</Text>
                <Text>Ciudad: {this.props.user.user.ciudad}</Text>
                <Text>Corregimiento: {this.props.user.user.corregimiento}</Text>
                <Text>Barriada: {this.props.user.user.barriada}</Text>
                <Text>Calle: {this.props.user.user.calle}</Text>
                <Text>Casa: {this.props.user.user.casa}</Text>
                <Text>PH: {this.props.user.user.ph}</Text>
                <Text>Apartamento: {this.props.user.user.apartamento}</Text>
                <Text>Aclaracion: {this.props.user.user.aclaracion}</Text>
                <Button
                  onClick={() => this.setState({ modalresidencia: true })}
                  type="primary"
                >
                  Editar
                </Button>
              </Space>
            </Col>
          </Row>

          <Row className="tarjetas">
            <Col span={24}>
              <Space direction="vertical">
                <Title level={4}>Mi matriculador y patrocinador</Title>
                <Text>
                  Matriculador:{" "}
                  {this.props.user.user.Matriculador
                    ? this.props.user.user.matri.name
                    : null}
                </Text>
                <Text>
                  Telefono:
                  {this.props.user.user.Matriculador
                    ? this.props.user.user.matri.numcelularllamadas
                    : null}
                </Text>
                <Text>
                  Patrocinador:{" "}
                  {this.props.user.user.Patrocinador
                    ? this.props.user.user.patri.name
                    : null}
                </Text>
                <Text>
                  Telefono:
                  {this.props.user.user.Patrocinador
                    ? this.props.user.user.patri.numcelularllamadas
                    : null}
                </Text>
                <Button
                  onClick={() => this.setState({ modalmatriculador: true })}
                  type="primary"
                >
                  Editar
                </Button>
              </Space>
            </Col>
          </Row>

          <Row className="tarjetas">
            <Col span={24}>
              <Space direction="vertical">
                <Title level={4}>Mis datos bancarios e identificacion </Title>
                <Text>
                  Tipo de documento:{" "}
                  {this.props.user.user.tipo_identificacion == 1
                    ? "Cedula"
                    : this.props.user.user.tipo_identificacion == 1
                    ? "Pasaporte"
                    : null}
                </Text>
                <Text>
                  Numero de documento: {this.props.user.user.numidentificacion}
                </Text>
                <Text>Banco: {this.props.user.user.banco}</Text>
                <Text>
                  Numero de cuenta: {this.props.user.user.numerocuenta}
                </Text>
                <Button
                  onClick={() => this.setState({ modalbancarios: true })}
                  type="primary"
                >
                  Editar
                </Button>
              </Space>
            </Col>
          </Row>
          <EditDatoscontacto
            status={this.state.modalcontacto}
            cancel={this.cancelModal.bind(this)}
          />
          <EditDatosresidencia
            status={this.state.modalresidencia}
            cancel={this.cancelModal.bind(this)}
          />
          <EditDatosmatriculador
            status={this.state.modalmatriculador}
            cancel={this.cancelModal.bind(this)}
          />
          <EditDatosbanco
            status={this.state.modalbancarios}
            cancel={this.cancelModal.bind(this)}
          />
        </Layoutcuenta>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Micuenta);
