import React from "react";
import logo from "../static/images/logo.png";
import "../static/css/style.css";
import { Layout, Row, Col, Form, Input, Button, Modal } from "antd";
import MetaTags from "react-meta-tags";
import LayoutPagina from "../components/Layout/LayoutPagina";
import Subtitle from "../components/Web/Subtitle";
import Constants from "../utils/Constants";

const { Header, Content, Footer, Sider } = Layout;

class Noticias extends React.Component {
  state = {
    DataNews: [],
  };

  GetNews = (values) => {
    console.log(values);

    fetch(`${Constants.URL}/news?$limit=3`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ DataNews: res.data }));
  };

  componentDidMount() {
    this.GetNews();
  }

  render() {
    const news = this.state.DataNews;
    return (
      <LayoutPagina>
        <MetaTags>
          <title>Noticias | 3x12</title>
          <meta name="description" content="Noticias 3x12 Plataforma " />
          <meta property="og:title" content="Noticias Plataforma 3x12" />
        </MetaTags>
        <Subtitle subtitle="Noticias y Novedades" />
        <Row
          justify="center"
          className="text-center rn-blog-area rn-section-gap bg_color--1 espaciorow_mobile"
        >
          <Col span={24}>
            <div class="section-title section-title--2 text-center mb--20">
              <h2 class="title">Últimas noticias</h2>
              <p>Lista de noticias y novedades sobre 3x12.</p>
            </div>
          </Col>
        </Row>
        <Row justify="center" className="text-center" gutter={[48, 16]}>
          {news.map((noticia) => (
            <Col xs={12} sm={12} md={7} lg={7} xl={7}>
              <div class="blog blog-style--1">
                <div class="thumbnail">
                  <a href={"noticia/" + noticia.id}>
                    <img
                      class="w-100"
                      // src="http://3.19.39.85/mlm/public/web/assets/images/blog/blog-01.jpg"
                      src={`${Constants.URL}/images/news/${noticia.photo}`}
                      alt="Blog Images"
                    />
                  </a>
                </div>
                <div class="content">
                  <p class="blogtype">{noticia.news}</p>
                  <h4 class="title">
                    <a href={"noticia/" + noticia.id}>{noticia.news}</a>
                  </h4>
                  <div class="blog-btn">
                    <a class="rn-btn text-white" href={"noticia/" + noticia.id}>
                      Leer más
                    </a>
                  </div>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </LayoutPagina>
    );
  }
}

export default Noticias;
