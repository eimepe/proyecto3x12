import React, { useState, useEffect } from "react";
import logo from "../static/images/logo.png";
import "../static/css/registro.css";
import { connect } from "react-redux";
import { setUser } from "../redux/actions";
import {
  Layout,
  Typography,
  Divider,
  Button,
  Form,
  Input,
  Modal,
  Row,
  Col,
} from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import Subtitle from "../components/Web/Subtitle";
import Constants from "../utils/Constants";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { Redirect } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;
const { Text } = Typography;

const Registro = () => {
  const [form] = Form.useForm();
  const [response, setResponse] = useState(null);
  const [ok, setOk] = useState(false);
  const [id, setId] = useState(null);

  const ResetPassnew = (values) => {
    fetch(Constants.URL + "/users/" + id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        password: values.password,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          Modal.success({
            content: "Cambio de contraseña exitoso",
            onOk: () => {
              console.log("ok");
              setOk(true);
            },
          });
        } else {
          Modal.error({
            content: "Error Al cambiar la contraseña",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const resetCode = (values) => {
    fetch(Constants.URL + "/resetpass?code=" + values.codigo, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      /*  body: JSON.stringify({
      "email":this.state.email,
     }),
*/
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res.response);
        if (res.response == "1") {
          setId(res.id);
        } else {
          setResponse("El codigo no es valido o ya caduco intente de nuevo.");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  if (ok) {
    return <Redirect to="/login" />;
  }

  if (id == null) {
    return (
      <LayoutPagina>
        <div className="contenido-register">
          <h2 class="title">Codigo de recuperacion.</h2>
          <Text>
            Hemos enviado un codigo a tu correo electronico con el cual podras
            combiar tu contraseña
          </Text>
          <Form form={form} onFinish={resetCode} layout="horizontal">
            <Row justify="center" className="text-center" gutter={[48, 24]}>
              <Col xs={24} sm={24} md={14} lg={24} xl={24}>
                <Form.Item
                  rules={[
                    { required: true, message: "Este campo es obligatorio" },
                  ]}
                  name="codigo"
                  hasFeedback
                >
                  <Input size="large" placeholder="Codigo" />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item>
              <Button type="primary" size="large" htmlType="submit">
                Enviar Codigo
              </Button>
            </Form.Item>
          </Form>
        </div>
      </LayoutPagina>
    );
  } else {
    return (
      <LayoutPagina>
        <div className="contenido-register">
          <h2 class="title">Nueva Contraseña.</h2>

          <Form form={form} onFinish={ResetPassnew} layout="horizontal">
            <Row justify="center" className="text-center" gutter={[48, 24]}>
              <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                <Form.Item
                  rules={[
                    { required: true, message: "Este campo es obligatorio" },
                  ]}
                  name="password"
                  hasFeedback
                >
                  <Input.Password size="large" placeholder="Nueva Contraseña" />
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                <Form.Item
                  name="validatepass"
                  dependencies={["password"]}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: "Por favor confirmar contraseña!",
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        if (!value || getFieldValue("password") === value) {
                          return Promise.resolve();
                        }

                        return Promise.reject("las contraseñas no coinciden!");
                      },
                    }),
                  ]}
                >
                  <Input.Password
                    size="large"
                    placeholder="Validar Contraseña"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item>
              <Button type="primary" size="large" htmlType="submit">
                Cambiar Contraseña
              </Button>
            </Form.Item>
          </Form>
        </div>
      </LayoutPagina>
    );
  }
};

const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser: setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Registro);
