import React, { useState } from "react";
import logo from "../static/images/logo.png";
import fondo from "../static/images/finding-us-01.png";
import "../static/css/style.css";
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2,
} from "react-html-parser";
import { Layout, Menu, Breadcrumb, Button, Row, Col } from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import Constants from "../utils/Constants";
import { MetaTags } from "react-meta-tags";
import Subtitle from "../components/Web/Subtitle";
const { Header, Content, Footer, Sider } = Layout;

class Contenido extends React.Component {
  state = {
    contenido: {},
  };

  LoadContent = (url) => {
    const varurl = url.split("-");
    const varid = varurl[varurl.length - 1];

    fetch(`${Constants.URL}/contents/${varid}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ contenido: res }));
  };

  mostrarContenido = () => {
    const contenido = this.state.contenido;
    return (
      <React.Fragment>
        <MetaTags>
          <title>{contenido.title}  3x12</title>
          <meta name="description" content="{contenido.title} plataforma 3x12." />
          <meta property="og:title" content="{contenido.title} plataforma 3 x12" />
        </MetaTags>
        <LayoutPagina >
          <Col>
            <Subtitle subtitle={contenido.title} imagenfondo={`${Constants.URL}/images/content/${contenido.photo}`}/>
            <div className="contenido-register">
              <h2 class="title">{contenido.title}.</h2> 
              <Row className="text-center" gutter={[48, 24]}>
                <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                  {ReactHtmlParser(contenido.content)}
                </Col>
                <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                  <img
                      src={`${Constants.URL}/imagenes/content/${contenido.photo}`}
                    />
                </Col>
              </Row>
            </div>
          </Col>
          
        </LayoutPagina>
      </React.Fragment>
    );
  };
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.match.params.url !== prevProps.match.params.url) {
      this.LoadContent(this.props.match.params.url);
    }
  }

  componentDidMount() {
    this.LoadContent(this.props.match.params.url);
  }

  render() {
    return <React.Fragment>{this.mostrarContenido()}</React.Fragment>;
  }
}

export default Contenido;
