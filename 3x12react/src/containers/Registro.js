import React, { useState, useEffect } from "react";
import logo from "../static/images/logo.png";
import "../static/css/registro.css";
import { connect } from "react-redux";
import { setUser } from "../redux/actions";
import {
  Layout,
  Typography,
  Divider,
  Button,
  Form,
  Input,
  Modal,
  Row,
  Col,
} from "antd";
import LayoutPagina from "../components/Layout/LayoutPagina";
import Subtitle from "../components/Web/Subtitle";
import Constants from "../utils/Constants";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

const { Header, Content, Footer, Sider } = Layout;
const { Text } = Typography;

const Registro = () => {
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [response, setResponse] = useState("");
  const [subtitle, setSubtitle] = useState("Registro");
  const [facebookId, setFacebookId] = useState(null);
  const [nombres, setNombre] = useState("");
  const [btnfacetext, setBtnfacetext] = useState("Registrarme con facebook");

  const responseFacebook = (response) => {
    console.log(response);
    setFacebookId(response.id);
    setBtnfacetext("Hola: " + response.name);
    setResponse(
      "No logramos obtener todos los datos de facebook por favor termina de llenar el formulario"
    );
    form.setFieldsValue({
      nombres: response.name,
    });
  };
  const Registrouser = (values) => {
    console.log(values);
    var data = {
      name: values.nombres + " " + values.apellidos,
      mobile_no: values.celular,
      email: values.email,
      password: values.password,
      facebookId: facebookId,
      role: 0,
      username: values.email,
    };

    fetch(`${Constants.URL}/users`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        if (res.code == 400) {
          if (res.errors[0].path == "email") {
            Modal.error({
              content:
                "El Correo " +
                values.email +
                " Ya existe en nuestra plataforma",
            });
          } else if (res.errors[0].path == "face") {
            Modal.error({
              content:
                "Tu cuenta  " +
                facebookId +
                " de Facebook Ya existe en nuestra plataforma por favor inicia sesion",
            });
          }
        } else if (res.id > 0) {
          Modal.success({
            content: "Te has Registrado con exito ya puedes iniciar sesion",
          });
        }
      });
  };

  const handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  const tailLayout = {
    wrapperCol: { span: 25 },
  };
  return (
    <LayoutPagina>
      <Subtitle subtitle={subtitle} />
      <div className="contenido-register">
        <Row justify="space-around" gutter={[48, 24]}>
          <Col xs={24} sm={24} md={20} lg={20} xl={20}>
            <h2 class="title">Registrate.</h2>
          </Col>
        </Row>
        <Text>{facebookId}</Text>
        <Form form={form} onFinish={Registrouser} layout="horizontal">
          <Row justify="center" className="text-center" gutter={[48, 24]}>
            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                rules={[
                  { required: true, message: "Este campo es obligatorio" },
                ]}
                name="nombres"
                hasFeedback
              >
                <Input
                  size="large"
                  //defaultValue={this.state.nombre}

                  placeholder="Nombres"
                />
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                rules={[
                  { required: true, message: "Este campo es obligatorio" },
                ]}
                name="apellidos"
                hasFeedback
              >
                <Input size="large" placeholder="Apellidos" />
              </Form.Item>
            </Col>
          </Row>
          <Row justify="center" className="text-center" gutter={[48, 24]}>
            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                rules={[
                  { required: true, message: "Este campo es obligatorio" },
                ]}
                name="celular"
                hasFeedback
              >
                <Input size="large" placeholder="Celular" />
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: "Este campo es obligatorio",
                    type: "email",
                  },
                ]}
                name="email"
                hasFeedback
              >
                <Input size="large" placeholder="Email" />
              </Form.Item>
            </Col>
          </Row>

          <Row justify="center" className="text-center" gutter={[48, 24]}>
            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                rules={[
                  { required: true, message: "Este campo es obligatorio" },
                ]}
                name="password"
                hasFeedback
              >
                <Input.Password size="large" placeholder="Contraseña" />
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={10} lg={10} xl={10}>
              <Form.Item
                name="validatepass"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Por favir confirmar contraseña!",
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }

                      return Promise.reject("las contraseñas no coinciden!");
                    },
                  }),
                ]}
              >
                <Input.Password size="large" placeholder="Validar Contraseña" />
              </Form.Item>
            </Col>
          </Row>
          <Row justify="space-around" gutter={[48, 24]}>
            <Col xs={24} sm={24} md={20} lg={20} xl={20}>
              <Text>
                Al hacer clic en Registrarme, aceptas los{" "}
                <a
                  style={{ color: "blue" }}
                  target="_blank"
                  href="/terminosycondicines.pdf"
                >
                  Terminos y condiciones
                </a>
              </Text>
              <Form.Item>
                <Button type="primary" size="large" htmlType="submit">
                  Registrarme
                </Button>
              </Form.Item>

              <FacebookLogin
                appId="3431636803525474"
                autoLoad={false}
                callback={responseFacebook}
                render={(renderProps) => (
                  <Button type="danger" onClick={renderProps.onClick}>
                    {btnfacetext}
                  </Button>
                )}
              />
              <Text>{" " + response}</Text>
            </Col>
          </Row>
        </Form>
      </div>
    </LayoutPagina>
  );
};

const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser: setUser,
};

export default connect(mapStateProps, mapDispatchToProps)(Registro);
