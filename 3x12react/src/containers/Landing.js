import React from "react";
import logo from "../static/images/logo.png";
import img_queremos from "../static/images/icon-1.png";
import img_mision from "../static/images/icon-2.png";
import img_quienes from "../static/images/icon-3.png";
import check_blue from "../static/images/icon-4.png";
import check_red from "../static/images/icon-5.png";
import ico_mail from "../static/images/icon-7.png";
import ico_tel from "../static/images/icon-7.png";
import ico_pos from "../static/images/icon-6.png";
import ico_facebook from "../static/images/icon-9.png";
import ico_instagram from "../static/images/icon-8.png";
import img_sol from "../static/images/icon-11.png";
import img_lineas from "../static/images/icon-10.png";

import "../static/css/style.css";
import { Layout, Menu, Breadcrumb, Button, Row, Col } from "antd";

import LayoutLanding from "../components/Layout/LayoutLanding";
import ButtonAfiliar from "../components/Landing/ButtonAfiliar";
import MetaTags from "react-meta-tags";

const { Header, Content, Footer, Sider } = Layout;

class Landing extends React.Component {
  render() {
    return (
      <LayoutLanding>
        <MetaTags>
          <title>Plataforma 3x12</title>
          <meta name="description" content="3x12 Plataforma " />
          <meta property="og:title" content="Plataforma 3x12" />
        </MetaTags>
        <Content style={{ marginTop: 100 }}>
          <Row justify="center" className="text-center">
            <div className="text-center">
              <p className="title-312">¿Qué es 3x12?</p>
            </div>
          </Row>
          <Row justify="center" className="text-center" id="inicio">
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <img src={img_queremos} className="imgResponsive" />
              <h3>¿QUE QUEREMOS?</h3>
              <p className="text_info">
                Ayudarte a generar ingresos desde casa usando tu linea de
                teléfono como lo has hecho por tantos años, pero ahora pagandote
                por ello.
              </p>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <img src={img_mision} className="imgResponsive" />
              <h3>NUESTRA MISIÓN</h3>
              <p className="text_info">
                Brindarte una oportunidad de negocio que mejore tu economía de
                una forma fácil, segura y rápida, permitiendote alcanzar esas
                metas y sueños que te has propuesto
              </p>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <img src={img_quienes} className="imgResponsive" />
              <h3>¿QUIENES SOMOS?</h3>
              <p className="text_info">
                Somos una empresa que garantiza tu inversión desde el minuto 1,
                lo que por años nos ha diferenciado de todas las redes de
                mercadeo que conoces.
              </p>
            </Col>
          </Row>
          <ButtonAfiliar />

          <Row justify="center" className="text-center">
            <div className="text-center">
              <p className="title-312">¿Quienes pueden Afiliarse?</p>
              <br />
            </div>
          </Row>
          <Row
            justify="center"
            className="text-center "
            gutter={[48]}
            id="nosotros"
          >
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock" align={"middle"}>
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Emprendedores</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Desempleados</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Networkers</span>
                </Col>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Estudiantes Universitarios</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Vendedores</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Creadores de contenido</span>
                </Col>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Dependientes</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Independientes</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Viajeros</span>
                </Col>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Mayores de 18 años</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Influencers</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_blue} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">
                    Mujeres y Hombres residentes en Panamá
                  </span>
                </Col>
              </div>
            </Col>
            <div className="ol_shape8">
              <div className="animation">
                <img src={img_sol} className="floating_image" />
              </div>
            </div>

            <div className="ol_shape11">
              <div className="animation">
                <img
                  src={img_lineas}
                  className="floating_image"
                  style={{ minWidth: 150 }}
                />
              </div>
            </div>
          </Row>
          <ButtonAfiliar />

          <Row justify="center" className="text-center">
            <div className="text-center">
              <p className="title-312">¿Por que iniciar HOY?</p>
              <br />
            </div>
          </Row>
          <Row
            justify="center"
            className="text-center"
            gutter={[48]}
            id="oportunidades"
          >
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <h3>OFERTA ÚNICA</h3>
              <p className="text_info">
                Nunca antes te habían pagado por usar tu línea de celular y
                recargar como siempre.{" "}
                <span className="text_rojo"> Creamos una</span>
                <span className="text_rojo"> oportunidad de negocio</span>
                <span className="text_rojo"> rentable</span>, con grandes
                oportunidades de hacer crecer tu economía.
              </p>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <h3>NUESTRA MISIÓN</h3>
              <p className="text_info">
                Desde el primer momento en que eliges afiliarte ya te estamos
                garantizando el
                <span className="text_rojo"> retorno al 100% de tu</span>
                <span className="text_rojo"> inversión ÚNICA</span>. Así es, por
                un pago único y mínimo empiezas a generar ingresos.
              </p>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <h3>¿QUIENES SOMOS?</h3>
              <p className="text_info">
                <span className="text_rojo"> Garantizamos tu inversión </span>
                <span className="text_rojo"> desde el minuto 1</span>, lo que
                por años nos ha diferenciado de todas las redes de mercadeo que
                conoces. No necesitas experiencia y nosotros estaremos para
                guiarte en cada paso.
              </p>
            </Col>
          </Row>
          <ButtonAfiliar />

          <Row justify="center" className="text-center">
            <div className="text-center">
              <p className="title-312">¿Quienes pueden Afiliarse?</p>
              <br />
            </div>
          </Row>
          <Row justify="center" className="text-center " gutter={[48]}>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Viajes</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Bonos en efectivo</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes ">Crecimiento</span>
                </Col>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Cuota para vehículo</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Negocio rentable</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Superación personal</span>
                </Col>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Liberta económica</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Independencia Laboral</span>
                </Col>
              </div>
              <div className="check_text">
                <Col span={4} className="inlineBlock">
                  <img src={check_red} className="img_check" />
                </Col>
                <Col span={20} className="inlineBlock">
                  <span className="quienes">Comisiones</span>
                </Col>
              </div>
            </Col>
          </Row>
          <ButtonAfiliar />

          <Row justify="center" className="text-center ">
            <div className="text-center">
              <p className="title-312 margin_title">¿Aún no te decides?</p>
              <p>
                <span className="text_rojo_title">
                  Estamos para responder todas tus consultas y<br />
                  ayudarte a crecer
                </span>
              </p>
            </div>
          </Row>
          <Row
            justify="center"
            className="text-center "
            gutter={[48]}
            id="contactenos"
          >
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <img src={ico_mail} className="imgResponsive" />
                <p class="text_info">
                  <a href="mailto:info@3x12panama.net">info@3x12.net</a>
                </p>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <img src={ico_tel} className="imgResponsive" />
                <p class="text_info">
                  <a href="tel:+50760901234">+507 60901234</a>
                </p>
              </div>
            </Col>
            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
              <div className="check_text">
                <img src={ico_pos} className="imgResponsive" />
                <p class="text_info">
                  <a href="map:Panamá, Vía España">Panamá, Vía España</a>
                </p>
              </div>
            </Col>
          </Row>
          <Row justify="center" className="text-center ">
            <div id="conten-bloque-line-height">
              <Col
                xs={24}
                sm={24}
                md={6}
                lg={6}
                xl={6}
                className="bloque-line-height"
              >
                <img src={ico_facebook} style={{ width: 50 }} />
                <span class="redes">3x12 Panamá</span>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={6}
                lg={6}
                xl={6}
                className="bloque-line-height"
              >
                <img src={ico_instagram} style={{ width: 50 }} />
                <span class="redes">3x12panama</span>
              </Col>
            </div>
          </Row>
          <ButtonAfiliar />
        </Content>
      </LayoutLanding>
    );
  }
}

export default Landing;
