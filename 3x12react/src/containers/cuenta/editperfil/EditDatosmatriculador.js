import React, { Component } from "react";
import Modal from "antd/lib/modal/Modal";
import { connect } from "react-redux";
import { Form, Input, Button, Radio, Typography } from "antd";
import { setUser } from "../../../redux/actions";
import Constants from "../../../utils/Constants";
const { Text, Title } = Typography;
class EditDatosmatriculador extends Component {
  constructor() {
    super();
    this.state = {
      value: "",
      matri: { id: 0, name: "" },
      patri: { id: 0, name: "" },
      btndisable: true,
      automatri: null,
      autopatri: null,
      responsematri: "",
    };
  }
  setEditdata(values) {
    fetch(Constants.URL + "/users/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Matriculador: values.matriculador,

        Patrocinador: values.patrocinador,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          this.setMerber(values.matriculador);
          //this.props.cancel();
        } else {
          console.log("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setEditdataauto(values) {
    fetch(Constants.URL + "/users/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Matriculador: this.state.automatri,

        Patrocinador: this.state.autopatri,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          var users = this.props.user;
          users.user = res;
          this.props.setUser(users);
          this.setMerber(this.state.automatri);
          // this.props.cancel();
        } else {
          console.log("error al guardar");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setMerber(user) {
    fetch(Constants.URL + "/member-log", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: this.props.user.user.id,
        name: this.props.user.user.name,
        parent_id: user,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          // this.props.cancel();
        } else {
          console.log("error al guardar");
          if (res.errors[0].type == "unique violation") {
            this.updateMermber(user);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  updateMermber(user) {
    fetch(Constants.URL + "/member-log/" + this.props.user.user.id, {
      method: "PATCH",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: this.props.user.user.name,
        parent_id: user,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);

        if (res.id) {
          this.props.cancel();
        } else {
          console.log("error al guardar");
          if (res.errors[0].type == "unique violation") {
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  onChange = (e) => {
    console.log("radio checked", e.target.value);
    this.setState({
      value: e.target.value,
    });
  };

  validate3(user) {
    fetch(Constants.URL + "/member-log?parent_id=" + user.id, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(user.id);
        if (
          user.id !== this.props.user.user.id &&
          user.Matriculador !== this.props.user.user.id
        ) {
          if (res.data.length >= 3) {
            console.log("mayor");
            this.setState({
              responsematri: "Este Matriculador ya tiene 3 referidos",
              matri: { id: 0 },
            });
          } else {
            console.log("menos");
            this.setState({ matri: user });
            this.setState({ btndisable: false });
          }
        } else {
          console.log("no puede ser tu codigo");
          this.setState({
            responsematri: "No puede ser tu codigo o un codigo bajo su nivel",
            matri: { id: 0 },
          });
          this.setState({ btndisable: true });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  validatePatrocinador(user) {
    if (
      user.Patrocinador === this.props.user.user.id &&
      user.Matriculador === this.props.user.user.id
    ) {
      this.setState({
        responsepatri: "No puede ser un usuario bajo tu nivel",
        patri: { id: 0 },
        btndisable: true,
      });
    } else {
      this.setState({ btndisable: false });
    }
  }

  onChangeData(ev) {
    //  console.log(ev.target.value);

    fetch(Constants.URL + "/users/" + ev.target.value, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.code !== 404) {
          if (res.id > 0) {
            this.validate3(res);
          } else {
            this.setState({
              responsematri: "Codigo No valido",
              matri: { id: 0 },
              btndisable: true,
            });
          }
        } else {
          this.setState({
            responsematri: "El codigo no existe",
            matri: { id: 0 },
            btndisable: true,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onChangeDatapatri(ev) {
    //  console.log(ev.target.value);

    fetch(Constants.URL + "/users/" + ev.target.value, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.id !== this.props.user.user.id) {
          this.setState({ patri: res });
          if (res.id > 0) {
            this.validatePatrocinador(res);
            // this.setState({ btndisable: false });
          } else {
            this.setState({
              responsepatri: "Codigo no Valido",
              patri: { id: 0 },
              btndisable: true,
            });
          }
        } else {
          this.setState({
            responsepatri: "No puede ser tu codigo",
            patri: { id: 0 },
            btndisable: true,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onautoMatri(ev) {
    //  console.log(ev.target.value);

    fetch(Constants.URL + "/users?$limit=2&$sort[id]=-1", {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: this.props.user.accessToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (
          this.props.user.user.id !== res.data[1].id &&
          this.props.user.user.id !== res.data[1].Matriculador
        ) {
          this.setState({
            automatri: res.data[1].id,
            autopatri: res.data[1].id,
          });
        } else if (
          this.props.user.user.id !== res.data[0].id &&
          this.props.user.user.id !== res.data[0].Matriculador
        ) {
          this.setState({
            automatri: res.data[0].id,
            autopatri: res.data[0].id,
          });
        } else if (
          this.props.user.user.id !== res.data[2].id &&
          this.props.user.user.id !== res.data[2].Matriculador
        ) {
          this.setState({
            automatri: res.data[2].id,
            autopatri: res.data[2].id,
          });
        } else if (
          this.props.user.user.id !== res.data[3].id &&
          this.props.user.user.id !== res.data[3].Matriculador
        ) {
          this.setState({
            automatri: res.data[3].id,
            autopatri: res.data[3].id,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.onautoMatri();
  }
  render() {
    console.log(this.state.matri);
    // console.log(this.props.status);
    var context = this;
    return (
      <Modal
        title="Editar datos de Matriculador"
        visible={this.props.status}
        onCancel={this.props.cancel}
        footer={[
          <Button key="back" onClick={this.props.cancel}>
            Cancelar
          </Button>,
        ]}
      >
        <Text>Tienes un Matriculador</Text>
        <br />
        <Radio.Group onChange={this.onChange} value={this.state.value}>
          <Radio value={"SI"}>SI</Radio>
          <Radio value={"NO"}>NO</Radio>
        </Radio.Group>
        <br />
        <br />
        {this.state.value == "SI" ? (
          <Form name="basic" onFinish={this.setEditdata.bind(this)}>
            <Text>
              {this.state.matri.id > 0
                ? "Nombre :" + this.state.matri.name
                : this.state.matri.id == 0
                ? this.state.responsematri
                : this.state.responsematri}
            </Text>
            <Form.Item
              label="Codigo Matriculador"
              name="matriculador"
              initialValue={this.props.user.user.Matriculador}
              rules={[
                {
                  required: true,
                  message: "Este campo no puede estar vacio",
                },
              ]}
            >
              <Input onChange={this.onChangeData.bind(this)} />
            </Form.Item>
            <Text>
              {this.state.patri.id > 0
                ? "Nombre :" + this.state.patri.name
                : this.state.patri.id == 0
                ? this.state.responsepatri
                : this.state.responsepatri}
            </Text>
            <Form.Item
              label="Codigo Patrocinador"
              name="patrocinador"
              initialValue={this.props.user.user.Patrocinador}
            >
              <Input onChange={this.onChangeDatapatri.bind(this)} />
            </Form.Item>

            <Form.Item>
              <Button
                disabled={this.state.btndisable}
                type="primary"
                htmlType="submit"
              >
                Enviar
              </Button>
            </Form.Item>
          </Form>
        ) : this.state.value == "NO" ? (
          <>
            <Title level={4}>Has elegido que no tienes un matriculador </Title>
            <Button onClick={this.setEditdataauto.bind(this)} type="primary">
              Enviar
            </Button>
          </>
        ) : null}
      </Modal>
    );
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {
  setUser,
};

export default connect(
  mapStateProps,
  mapDispatchToProps
)(EditDatosmatriculador);
