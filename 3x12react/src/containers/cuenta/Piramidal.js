import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import { Layout, Space, Menu, Breadcrumb, Button } from "antd";
import OrganizationChart from "@dabeng/react-orgchart";
import MyNode from "../../components/MyNode";
import {
  UserOutlined,
  LaptopOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { connect } from "react-redux";

import Layoutcuenta from "../../components/Layout/Layoutcuenta";
import Constants from "../../utils/Constants";
import { Redirect } from "react-router-dom";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
class Piramidal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clase: "ocultar",
      ds: {},
      dss: {},
    };
    this.getdata = this.getdata.bind(this);
  }

  abrir() {}

  getdata() {
    fetch(
      `${Constants.URL}/estructuras?parent_id=${this.props.user.user.id}&createdAt: -1`,
      {
        method: "GET",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((res) => {
        //console.log(res);
        const array = {
          id: this.props.user.user.id,
          title: "Codigo:" + this.props.user.user.id,
          name: this.props.user.user.name,
          status: this.props.user.user.ifpago,
          image: this.props.user.user.picture_url,
          children: res.data,
        };

        this.setState({ ds: array });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getdata();
  }
  render() {
    if (!this.props.user.accessToken) {
      return <Redirect to="/" />;
    } else {
      return (
        <Layoutcuenta context={this.props.history}>
          {this.props.user.user.id ? (
            <OrganizationChart
              datasource={this.state.ds}
              chartClass="myChart"
              NodeTemplate={MyNode}
              pan={true}
              zoom={true}
            />
          ) : (
            "No hola"
          )}
        </Layoutcuenta>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(Piramidal);
