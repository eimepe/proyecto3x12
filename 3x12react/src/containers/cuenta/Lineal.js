import React from "react";
import logo from "../../static/images/logo.png";
import "../../static/css/style.css";
import { Layout, Space, Menu, Breadcrumb, Button, Tree, Switch } from "antd";
import OrganizationChart from "@dabeng/react-orgchart";
import MyNode from "../../components/MyNode";
import {
  UserOutlined,
  LaptopOutlined,
  SettingOutlined,
  CarryOutOutlined,
  FormOutlined,
} from "@ant-design/icons";
import { connect } from "react-redux";

import Layoutcuenta from "../../components/Layout/Layoutcuenta";
import Constants from "../../utils/Constants";
import { Redirect } from "react-router-dom";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
class Lineal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clase: "ocultar",
      ds: {},
      dss: {},
      showIcon: false,
      showLine: false,
      treeData: [],
    };
    this.getdata = this.getdata.bind(this);
  }

  abrir() {}

  getdata() {
    fetch(`${Constants.URL}/estructural?parent_id=${this.props.user.user.id}`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        const array = [
          {
            key: this.props.user.user.id,
            title: this.props.user.user.name,
            name: this.props.user.user.name,
            image:
              `${Constants.URL}/images/profile/` +
              this.props.user.user.picture_url,
            children: res.data,
          },
        ];

        this.setState({ treeData: array });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getdata();
  }
  render() {
    if (!this.props.user.accessToken) {
      return <Redirect to="/" />;
    } else {
      return (
        <Layoutcuenta context={this.props.history}>
          <Tree
            showLine={this.showLine}
            showIcon={this.stateshowIcon}
            treeData={this.state.treeData}
          />
        </Layoutcuenta>
      );
    }
  }
}
const mapStateProps = (state) => {
  return {
    user: state.user,
    //categorias: state.categorias,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateProps, mapDispatchToProps)(Lineal);
