import React from "react";
import logo from "../static/images/logo.png";
import img_queremos from "../static/images/icon-1.png";
import img_mision from "../static/images/icon-2.png";
import img_quienes from "../static/images/icon-3.png";
import check_blue from "../static/images/icon-4.png";
import check_red from "../static/images/icon-5.png";
import ico_mail from "../static/images/icon-7.png";
import ico_tel from "../static/images/icon-7.png";
import ico_pos from "../static/images/icon-6.png";
import ico_facebook from "../static/images/icon-9.png";
import ico_instagram from "../static/images/icon-8.png";
import img_sol from "../static/images/icon-11.png";
import img_lineas from "../static/images/icon-10.png";

import "../static/css/style.css";
import "../static/css/page.css";
import { Layout, Menu, Breadcrumb, Button, Row, Col, Carousel } from "antd";

import LayoutHome from "../components/Layout/LayoutHome";
import ButtonAfiliar from "../components/Landing/ButtonAfiliar";

import Seccion1 from "../components/Web/Seccion1";
import SeccionFinal from "../components/Web/SeccionFinal";
import NoticiasInicio from "../components/Web/NoticiasInicio";
import { MetaTags } from "react-meta-tags";


const { Header, Content, Footer, Sider } = Layout;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
    document.title = "Bienvenido | 3x12";
  }

  render() {
    return (
      <LayoutHome>
        <MetaTags>
          <title>Bienvenido a 3x12</title>
          <meta name="description" content="Bienvenido plataforma 3x12." />
          <meta property="og:title" content="plataforma 3 x12" />
        </MetaTags>
        <Seccion1 />
        
        <NoticiasInicio />
        <SeccionFinal />

        {/* <Content></Content> */}
      </LayoutHome>
    );
  }
}

export default Home;
